<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class My_Controller extends CI_Controller {

	public $data = array();

	function __construct(){
		parent::__construct();
	}
}


/**
 * for Backend develop
 */
class Backend extends My_Controller{
	
	function __construct(){
		parent::__construct();
		
		// Load helpers
        $this->load->helper('form');
        $this->load->helper('ip');
        $this->load->helper('notification');
        $this->load->helper('methods');

        // Load libraries
        $this->load->library('form_validation');
        $this->load->library('session');
        $this->load->library('controllerList');
        

        // Load models
        $this->load->model('action');


        // Login check
        $exception_uris = array(
            'access/login',
            'access/login/logout'
        );

        //print_r(uri_string());

        
        if(in_array(uri_string(), $exception_uris) == FALSE) {
            if($this->session->userdata('loggedIn') != true) {
                redirect('access/login');
            }
        }

        // set global data
        $this->data['controllers'] = $this->controllerlist->getControllers(); 
        $this->data['userid'] = $this->session->userdata('user_id');
        $this->data['user_privilege'] = $this->session->userdata('privilege');
        
	}



    /**
     * constuct full template page 
     * @param  [string] $page [path]
     * @return [type]       [description]
     */
    function load_page($page){

        $this->load->view('includes/header', $this->data, FALSE);
        $this->load->view('includes/header_menu', $this->data, FALSE);
        $this->load->view('includes/sidebar', $this->data, FALSE);
        $this->load->view(''.$page, $this->data, FALSE);
        $this->load->view('includes/footer', $this->data, FALSE);
    }

}


/**
 * for manage frontend page
 */
class Frontend extends My_Controller{
    
    function __construct(){
        parent::__construct();
        
        // Load helpers
        $this->load->helper('form');
        //$this->load->helper('ip');
        $this->load->helper('notification');
        $this->load->helper('methods');

        // load libraries
        $this->load->library('session');
        
        // Load models
        $this->load->model('action');

        // read site info
        $this->data['site_info'] = $this->action->read('website_setting');

        // read all depts for header menu
        $where = array('trash' => 0);
        $this->data['allDepts'] = $this->action->read('all_dept', $where);
    }


    /**
     * constuct full template page 
     * @param  [string] $page [path]
     * @return [type]       [description]
     */
    function load_view($page){

        $this->load->view('public/header', $this->data, FALSE);
        $this->load->view('public/header_menu', $this->data, FALSE);
        $this->load->view('public/slider', $this->data, FALSE);
        $this->load->view(''.$page, $this->data, FALSE);
        $this->load->view('public/footer', $this->data, FALSE);
    }


}







/* End of file My_Controller.php */
/* Location: ./application/core/My_Controller.php */
