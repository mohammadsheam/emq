<?php

	$config['api_key'] = 'pharmacy_store@sunnahsoft';

	$config['field_types'] = array(
		'text'     => 'text',
		'number'   => 'number',
		'checkbox' => 'checkbox',
		'file'     => 'file',
		'radio'    => 'radio',
	);

	$config['col_types'] = array(
		'VARCHAR' => 'varchar',
		'TEXT'    => 'text',
		'INT'     => 'int',
	);
	// for sidebar control
	$config['controllers'] = ['Dashboard', 'Auth', 'Myerror', 'Ajax', ];

    //unit
    $config['unit'] = array("Pcs", "Box", "Carton");

    // payment methods
    $config['payment_method'] = array('Cash','Bank');