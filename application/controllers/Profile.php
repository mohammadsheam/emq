<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends Backend {
	function __construct(){
		parent::__construct();
	}


	/**
	 * Create new profile
	 * @return [true] [success]
	 */
	public function index(){
		$this->data['title'] = lang('Profile')." | ".lang('Create_Profile');
		$this->data['notification'] = null;


		if($this->input->post('submit')){

			// load libraries
			$this->load->library('upload');

			$photo_path = '';

			$config['upload_path'] = './private/assets/images/users/';
            $config['allowed_types'] = 'png|jpeg|jpg|gif';
            $config['max_size'] = '5106';
            $config['max_width'] = '5000'; /* max width of the image file */
            $config['max_height'] = '5000';
            $config['file_name'] ="users_".date('Y-m-d').'_'.rand(1111,99999);
            $config['overwrite']=true;

            $this->upload->initialize($config);

            if ($this->upload->do_upload("image_file")){
                $upload_data = $this->upload->data();
                $photo_path = "private/assets/images/users/".$upload_data['file_name'];
            }

			$data = array(
				'date'      => $this->input->post('date'),
				'image'     => $photo_path,
				'fullname'  => $this->input->post('fullname'),
				'email'     => $this->input->post('email'),
				'username'  => $this->input->post('username'),
				'password'  => md5($this->input->post('pass')),
				'privilege' => $this->input->post('privilege'),
				'permission'=> json_encode(array())
			);
			
			$status = $this->action->add('users', $data);
			if($status){
				$msg = message('success', 'Profile successfully created!', 'Done');
			}else{
				$msg = message('error', 'Profile not created', 'Opps!');
			}

			$this->session->set_flashdata('notification', $msg);
			redirect('Profile/index','refresh');
		}

		$this->load_page('profile/create-profile');
	}

	
	/**
	 * Retrieve all profiles from database table
	 * @return [array datasets] 
	 */
	public function all(){
		$this->data['title'] = lang('Profile')." | ".lang('all');
		$this->data['notification'] = null;

		/*read profile info from database*/
		$where = array('trash' => 0);
		$this->data['results'] = $this->action->read('users', $where);

		if($this->input->post('update')){
			$where = array('id' => $this->input->post('id'));
			$data = array('password' => md5($this->input->post('new_pass')));

			$status = $this->action->update('users', $data, $where);
			
			if($status){
				$msg = message('success', 'Password has successfully changed!', 'Done');
			}else{
				$msg = message('error', 'Password not changed', 'Opps!');
			}

			$this->session->set_flashdata('notification', $msg);
			redirect('Profile/all','refresh');
		}

		$this->load_page('profile/all-profile');		
	}



	/**
	 * Profile Preview
	 */
	public function view()	{
		$this->data['title'] = lang('Profile');
		$this->data['notification'] = null;

		/*read profile info from database*/
		$where = array('id' => $this->session->userdata('user_id'));
		$this->data['info'] = $this->action->read('users', $where);

		$this->load_page('profile/myprofile');	
	}

	
	/**
	 * Create Update profile info
	 * @return [true] [success]
	 */
	public function edit($id){
		$this->data['title'] = lang('Profile')." | ".lang('Edit');
		$this->data['notification'] = null;


		// read info from database table
		$where = array('id' => $id);
		$this->data['record'] = $this->action->read('users', $where);

		if($this->input->post('update')){

			$data = array(
				'date'      => $this->input->post('date'),
				'fullname'  => $this->input->post('fullname'),
				'email'     => $this->input->post('email'),
				'username'  => $this->input->post('username'),
				'privilege' => $this->input->post('privilege')
			);


            if ($_FILES["image_file"]["name"]!=null && $_FILES["image_file"]["name"]!="" ) {
            	
				// load libraries
				$this->load->library('upload');

				$photo_path = '';

				$config['upload_path'] = './private/assets/images/users/';
	            $config['allowed_types'] = 'png|jpeg|jpg|gif';
	            $config['max_size'] = '5106';
	            $config['max_width'] = '5000'; /* max width of the image file */
	            $config['max_height'] = '5000';
	            $config['file_name'] ="users_".date('Y-m-d').'_'.rand(1111,99999);
	            $config['overwrite']=true;

	            $this->upload->initialize($config);

	            // delete previous image
				if (is_file('./'.$this->input->post('old_image'))) {
	                unlink('./'.$this->input->post('old_image'));
	            }

	            if ($this->upload->do_upload("image_file")){
	                $upload_data = $this->upload->data();
	                $data['image'] = "private/assets/images/users/".$upload_data['file_name'];
	            }
            }


			$where = array('id' => $id);
			$status = $this->action->update('users', $data, $where);
			if($status){
				$msg = message('success', 'Profile has successfully updated!', 'Done');
			}else{
				$msg = message('error', 'Profile not updated!', 'Opps!');
			}

			$this->session->set_flashdata('notification', $msg);
			redirect('Profile/all','refresh');
		}

		$this->load_page('profile/edit-profile');
	}


	public function delete($id)	{
		$where = array('id' => $id);
		$data = array('trash' => 1);
		$status = $this->action->update('users', $data, $where);
		if($status){
			$msg = message('success', 'Profile has successfully Deleted!', 'Done');
		}else{
			$msg = message('error', 'Profile not deleted!', 'Opps!');
		}

		$this->session->set_flashdata('notification', $msg);
		redirect('Profile/all','refresh');
	}

}

/* End of file profile.php */
/* Location: ./application/controllers/profile.php */