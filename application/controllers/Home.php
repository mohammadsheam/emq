<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends Frontend {

	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->data['title'] = lang('Admin_panel');
		$this->data['notification'] = null;

		


		
		$this->load_view('website/main_body');
	}


	public function department_view($dept_id){
		// read single dept info
		$from     = "dept_info";
		$join     = "all_dept";
		$joinCond = "dept_info.dept_id=all_dept.id";
		$where    = array('dept_info.id' => $dept_id);
		$records = $this->action->joinAndRead($from, $join, $joinCond, $where);

		if(!$records){
			redirect('home','refresh');
		}

		
		$this->data['dept_info'] = $records;
		//$this->load_view('website/dept_single');
		$this->load->view('public/header', $this->data, FALSE);
        $this->load->view('public/header_menu', $this->data, FALSE);
        //$this->load->view('public/slider', $this->data, FALSE);
        $this->load->view('website/dept_single', $this->data, FALSE);
        $this->load->view('public/footer', $this->data, FALSE);
	}


	public function left_sidebar(){
		echo $this->load->view('public/left_sidebar', $this->data, TRUE);
	}



}

/* End of file home.php */
/* Location: ./application/controllers/home.php */