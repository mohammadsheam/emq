<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Website_header extends Backend {

	function __construct(){
		parent::__construct();
		
		// load libraries
		$this->load->library('upload');
	}

	public function header(){
		$this->data['title'] = lang('Backup')." | ".lang('all');
		$this->data['notification'] = null;

		$this->data['record'] = $this->action->read('website_setting');

		if ($this->input->post('submit')) {

			// update logo
			$logo_path = $this->input->post('old_logo');
			if ($_FILES["logo"]["name"]!=null && $_FILES["logo"]["name"]!="" ) {
				$logo_path = $this->uploadImage('logo');
				unlink('./'.$this->input->post('old_logo'));
			}

			// update favicon
			$favicon = $this->input->post('old_fav');
			if ($_FILES["favicon"]["name"]!=null && $_FILES["favicon"]["name"]!="" ) {
				$favicon = $this->uploadImage('favicon');
				unlink('./'.$this->input->post('old_fav'));
			}			

			$header = array(
				'logo'       => $logo_path,
				'favicon'    => $favicon,
				'site_title' => $this->input->post('site_title'),
			);

			$data = array('date' => date('Y-m-d'),'header' => json_encode($header));

			$status = $this->action->update('website_setting', $data);

			if($status){
				$msg = message('success', 'Header data successfully updated!', 'Done');
			}else{
				$msg = message('error', 'Header data not updated', 'Opps!');
			}

			$this->session->set_flashdata('notification', $msg);
			redirect('Website_header/header','refresh');
		}

		$this->load_page('website_setting/header');
		
	}


	public function footer(){
		
	}

	public function sidebar(){
		
		$this->data['title'] = lang('Backup')." | ".lang('all');
		$this->data['notification'] = null;


		$this->load_page('backup/all_backup');
	}


	public function uploadImage($file_name){
		$photo_path = '';

		$config['upload_path'] = './public/assets/img/';
        $config['allowed_types'] = 'png|jpeg|jpg|gif';
        $config['max_size'] = '5106';
        $config['max_width'] = '5000'; /* max width of the image file */
        $config['max_height'] = '5000';
        $config['file_name'] =$file_name."_".date('Y-m-d').'_'.rand(1111,99999);
        $config['overwrite']=true;

        $this->upload->initialize($config);

        if ($this->upload->do_upload($file_name)){
            $upload_data = $this->upload->data();
            $photo_path = "public/assets/img/".$upload_data['file_name'];
        }

        return $photo_path;
	}

}

/* End of file Website_header.php */
/* Location: ./application/controllers/Website_header.php */