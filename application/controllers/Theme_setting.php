<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_setting extends Backend {

	function __construct(){
		parent::__construct();		
	}


	public function index(){
		$this->data['title'] = lang('Theme_setting')." | ".lang('add');
		$this->data['notification'] = null;

		/*read info from database table*/
		$where = array();
		$this->data['info'] = $this->action->read('theme_setting', $where);

		if($this->input->post('submit')){
			$data = array(
				//'theme_color' => $this->input->post('theme_color'),
				//'footer'      => $this->input->post('footer'),
				//'header'      => $this->input->post('header'),
				//'logo'        => $this->input->post('logo'),
				'language'    => $this->input->post('language'),
			);

			$status = $this->action->update('theme_setting', $data, array());
			if($status){
				$msg = message('success', 'Theme Data successfully Saved!', 'Done');
			}else{
				$msg = message('error', 'Something went wrong', 'Opps!');
			}

			$this->session->set_flashdata('notification', $msg);
			redirect('Theme_setting','refresh');

		}

		$this->load_page('theme_setting/add');
	}

}

/* End of file theme_setting.php */
/* Location: ./application/controllers/setting/theme_setting.php */