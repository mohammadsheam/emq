<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends Backend {

	function __construct(){
		parent::__construct();
	}


	public function index(){
		$this->data['error'] = NULL;

		if ($this->input->post('login')) {
			$user = $this->input->post('user');
			$pass = md5( $this->input->post('pass') );

			$where = array(
				'username' => $user,
				'password' => $pass
			);

			$info = $this->action->read('users', $where);

			//print_r($info);

			if($info){
				$data = array(
					'user_id'      => $info[0]->id,
					'username'     => $info[0]->username,
					'fullname'     => $info[0]->fullname,
					'privilege'    => $info[0]->privilege,
					'image'        => $info[0]->image,
					'login_period' => date('Y-m-d H:i:s a'),
					'loggedIn'     => true
				);

				$this->session->set_userdata($data);


				// store access information
				$ip = get_client_ip();
				$os = getOS();
				$browser = getBrowser();
				$device = getDevice();


	            $data = array(
					'date'       => date('Y-m-d'),
					'username'   => $this->session->userdata('username'),
					'browser'    => $browser,
					'os'         => $os,
					'ip'         => $ip,
					'device'     => $device,
					'user_id'    => $info[0]->id,
					'login_time' => $this->session->userdata('login_period')
	            );

	            $msg = message('success', 'Welcome to Dashboard!', 'Welcome '.$info[0]->fullname);
	            $this->session->set_flashdata('notification', $msg);
	            $this->db->insert("access_info", $data);
				redirect('admin','refresh');

			}else{

				$user = $this->input->post('user');
				$pass = $this->input->post('pass');

				// check developer access
				$this->load->config('my_access');
				$super = config_item('developer');

				if($super['username'] == $user && $super['password'] == $pass){
					$data = array(
						'user_id'      => 69,
						'username'     => 'developer',
						'fullname'     => 'Sunnah Soft',
						'privilege'    => 'superadmin',
						'image'        => 'private/assets/images/users/1.jpg',
						'login_period' => date('Y-m-d H:i:s a'),
						'loggedIn'     => true
					);

					$this->session->set_userdata($data);


					// store access information
					$ip = get_client_ip();
					$os = getOS();
					$browser = getBrowser();
					$device = getDevice();


		            $data = array(
						'date'       => date('Y-m-d'),
						'username'   => $this->session->userdata('username'),
						'browser'    => $browser,
						'os'         => $os,
						'ip'         => $ip,
						'device'     => $device,
						'user_id'     => 69,
						'login_time' => $this->session->userdata('login_period')
		            );

		            $msg = message('success', 'Welcome to Dashboard!', 'Welcome '.'Sunnah Soft');
		            $this->session->set_flashdata('notification', $msg);
		            $this->db->insert("access_info", $data);
					redirect('admin','refresh');

				}else{
					$this->data['error'] = "Username or Password is incorrect";
				}
			}
		}

		$this->load->view('access/login', $this->data, FALSE);
	}


	public function logout(){
		// updates access info
        
        $where = array(
            'user_id'       =>$this->session->userdata('user_id'),
            'login_time'  => $this->session->userdata('login_period')
        );
        $this->db->set(array('logout_time' => date('Y-m-d H:i:s a')));
        $this->db->where($where);
        $this->db->update("access_info");

        $this->session->sess_destroy();

		redirect('access/login','refresh');
	}

}

/* End of file login.php */
/* Location: ./application/controllers/access/login.php */