<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Developer_setting extends Backend {

	function __construct(){
		parent::__construct();
		// load libray
		$this->load->library('controllerlist');
	}


	public function index(){
		$this->data['title'] = lang('Developer_setting')." | ".lang('add');
		$this->data['notification'] = null;

		if($this->input->post('submit')){
			
			// receive from POST method 
			$conName   = str_replace(" ", "_",trim($this->input->post('controller'), " "));
			$tableName = str_replace(" ", "_",trim($this->input->post('table'), " "));
			$bn        = $this->input->post('bncontroller');
			

			// read all controllers
			$all_controllers = $this->controllerlist->getControllers();
			
			if(! array_key_exists($conName, $all_controllers)){

				// create dynamic controller
				$controller = generateClass($conName, $tableName, $_POST['colname']);


				// add language item
				$config_lang = config_lang(ucfirst($conName), $conName, $bn);

				foreach($_POST['colname'] as $key => $col){
					$lebel = str_replace(" ", "_",trim($col, " "));
					config_lang($lebel, $col, $_POST['colbname'][$key]);
				}


				// create dynamic view
				$view = generateView($conName, $_POST['colname'], $_POST['field_type']);
				
				// create dynamic table
				$model = $this->generateTable($tableName, $_POST['colname'], $_POST['coltype'], $_POST['colsize']);

				//echo "<pre>"; print_r($controller); print_r($view); print_r($config_lang);echo "</pre>";

				if($controller==true && $model==true && $view==true && $config_lang==true){
					$msg = message('success', 'Everything created successfully!', 'Done');
				}else{
					$msg = message('error', 'Error in generating controller, view, table', 'Opps!');
				}

			}else{
				$msg = message('error', 'Controller Already Exists', 'Opps!');
			}

			$this->session->set_flashdata('notification', $msg);
			redirect('Developer_setting','refresh');
		}

		$this->load_page('developer_setting/add');

	}


	/**
	 * create table with column description
	 * @param  [string] $table   [table name]
	 * @param  [array] $colname [column name]
	 * @param  [array] $coltype [column data type]
	 * @param  [array] $colsize [column data size]
	 * @return [boolean]          [true/false]
	 */
	function generateTable($table, $colname, $coltype, $colsize){

		// load forge class
		$this->load->dbforge();
		
		// define table fields
		$fields = array(
			'id' => array(
				'type'           => 'INT',
				'constraint'     => 9,
				'unsigned'       => TRUE,
				'auto_increment' => TRUE
			)
		);

		foreach($colname as $key => $label){
			$fields[str_replace(" ","_",trim($label, " "))] = array(
				'type'       => $coltype[$key],
				'constraint' => $colsize[$key],
			);
		}

		$this->dbforge->add_field($fields);

		// define primary key
		$this->dbforge->add_key('id', TRUE);

		// create table
		$this->dbforge->create_table($table, TRUE);

		return true;
	}

}
