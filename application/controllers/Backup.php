<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Backup extends Backend {
	function __construct()	{
		parent::__construct();
		$this->dir = './backup/';
	}
	

	public function index()	{
		$this->data['title'] = lang('Backup')." | ".lang('add');
		$this->data['notification'] = null;
		
		$this->load_page('backup/backup');    
	}


	/**
	 * Create backup
	 * @param  [string] $type [project or database]
	 * @return [type]       [success]
	 */
	function createBackup($type=null){
		$this->data['notification'] = null;
		$this->load->library('zip');

		if($type == 'data'){
			$this->load->dbutil();
					    
		    if ( !file_exists( $this->dir ) ){
		    	mkdir($this->dir, 0777, true);
			}
			
			$db_format = array('format'=>'zip', 'filename'=>'db_backup.sql');
			$backup = $this->dbutil->backup($db_format);
			$dbname='database_'.date('Y-m-d').'.zip';
			$save = $this->dir.$dbname;
			write_file($save, $backup);

			
			$msg = message('success', 'Database has successfully backup!', 'Done');
			
			$this->session->set_flashdata('notification', $msg);
			redirect('Backup/all','refresh');

		}


		if ($type== 'project') {
		    if ( !file_exists( $this->dir ) ){
		    	mkdir($this->dir, 0777, true);
			}

			// delete previous file
			$file = 'project_'.date('Y-m-d').'.zip';
			if (is_file($this->dir.$file)) {
	            unlink($this->dir.$file);
	        }

			$this->load->library('Zip');
			$this->zip->read_dir(FCPATH, FALSE);
			$this->zip->archive($this->dir.'project_'.date('Y-m-d').'.zip');

			$msg = message('success', 'Project has successfully backup!', 'Done');
			
			$this->session->set_flashdata('notification', $msg);
			redirect('Backup/all','refresh');
		}
	
	}


	/**
	 * Fetch backup files from the project folder
	 * @return [type] [data array]
	 */
	public function all(){
		$this->data['title'] = lang('Backup')." | ".lang('all');
		$this->data['notification'] = null;
		$this->data['fileLists'] = null;

		$this->load->helper('directory');
		

		if ( file_exists( $this->dir ) ){
			$this->data['fileLists'] = directory_map($this->dir);
		}

		$this->load_page('backup/all_backup');
	}


	/**
	 * delete file
	 * @param  [string] $fname [file name]
	 * @return [type]        [success msg]
	 */
	public function delete($fname){
		$path = 'backup/'.$fname;

		if (file_exists($path)){
		
			if (unlink($path)) {
				$msg = message('success', 'File has successfully deleted!', 'Done');
				
				$this->session->set_flashdata('notification', $msg);
				redirect('Backup/all','refresh');
			}else{
				$msg = message('error', 'Something went wrong', 'Done');
				
				$this->session->set_flashdata('notification', $msg);
				redirect('Backup/all','refresh');
			}
		}else{

			$msg = message('success', 'File has successfully deleted!', 'Done');
				
			$this->session->set_flashdata('notification', $msg);
			redirect('Backup/all','refresh');
		}

	}

}

/* End of file backup.php */
/* Location: ./application/controllers/backup/backup.php */
