<?php

/**
 * Manage User role
 */
class User_permission extends Backend{
	
	function __construct(){
		parent::__construct();
	}


	function all(){
		$this->data['title'] = lang('User_permission');
		$this->data['notification'] = null;

		$this->data['user_id'] = $this->session->userdata['user_id'];

		// read all users
		$where = array('trash' => 0);
		$this->data['users'] = $this->action->read('users', $where);

		// read all modules
		$this->data['controllers'] = $this->controllerlist->getControllers(); 


		$this->load_page('user_permission/all');
	}


	function setPermission($id){
		$this->data['title'] = lang('User_permission')." | ".lang('permission');;
		$this->data['notification'] = null;

		// read all users
		$where = array('id' => $id);
		$this->data['users'] = $this->action->read('users', $where);

		// read all controllers
		$this->data['controllers'] = $this->controllerlist->getControllers();

		if($this->input->post('submit')){
			$data = array();
			if(isset($_POST['con'])){
				foreach ($_POST['con'] as $key => $value) {
					$data[$key]['module_name'] = $value;

					if(isset($_POST['insert'][$key])){
						$data[$key]['insert'] = true;
					}else{
						$data[$key]['insert'] = false;
					}

					if(isset($_POST['view'][$key])){
						$data[$key]['view'] = true;
					}else{
						$data[$key]['view'] = false;
					}

					if(isset($_POST['edit'][$key])){
						$data[$key]['edit'] = true;
					}else{
						$data[$key]['edit'] = false;
					}

					if(isset($_POST['delete'][$key])){
						$data[$key]['delete'] = true;
					}else{
						$data[$key]['delete'] = false;
					}
				}
			}

			//print_r(json_encode($data));
			$data = array('permission' => json_encode($data));
			$where = array('id' => $id);

			$status = $this->action->update('users', $data, $where);
			if($status){
				$msg = message('success', 'User Permission Successfully Saved!', 'Done');
			}else{
				$msg = message('error', 'Something went wrong', 'Opps!');
			}

			$this->session->set_flashdata('notification', $msg);
			redirect('User_permission/all','refresh');

		}

		$this->load_page('user_permission/allModule');
	}
}