<?php

defined('BASEPATH') or exit('No Direct Script allow');

class Activity extends Backend{

    function __construct(){
        parent::__construct();
    }

    public function all(){
        $this->data['title'] = lang('Activity');

        // read all user
        $where = array('trash' => 0);
        $this->data['users'] = $this->action->read('users', $where);

        $where = array('user_id' => $this->session->userdata('user_id'));
        // after search
        if($this->input->post('submit')){

            $where = array('user_id' => $this->input->post('user_id'));

            if(isset($_POST['search'])){
                foreach ($_POST['search'] as $key=> $value){
                    if($value != null && $key=='dateFrom'){
                        $where['date >='] = $value;
                    }
                    if($value != NULL && $key == "dateTo"){
                        $where['date <='] = $value;
                    }
                }
            }
        }
        // by default read current user data
        $this->data['actions'] = $this->action->read('activity', $where);
        $this->data['logs'] = $this->action->read('access_info', $where);

        $this->load_page('activity/all');
    }
}