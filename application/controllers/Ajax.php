<?php
//defined('BASEPATH') OR exit('No Direct Access is Allowed');

class Ajax extends Backend{
    function __construct() {
        parent::__construct();
        $this->load->helper('ajax');
    }

    // clients Balance
    public function partyInfo(){

        // get the incoming object
        $content = file_get_contents("php://input");

        // convert object to array
        $condition = json_decode($content, true);

        // take table name from the array
        $table = $condition['table'];

        if(array_key_exists('cond', $condition)){

            // read data from database
            $where = $condition['cond'];
            $info = $this->action->read($table, $where);

            $data = array(
                'info' => $info,
                'balance' => currentBalance($condition['id'])
            );

            echo json_encode($data);
        }
    }



    public function read(){
        // get the incoming object
        $content = file_get_contents("php://input");

        // convert object to array
        $receive = json_decode($content, true);

        // take table name from the array
        $table = $receive['table'];

        // set a default condition
        $condition = array();

        // check the condition exists into the array
        if(array_key_exists('cond', $receive)){
            $condition = $receive['cond'];
        }

        // get information from database
        $result = $this->action->read($table, $condition);

        // convert the information array to JSON string
        echo json_encode($result);
    }



    public function update(){

        // get the incoming object
        $content = file_get_contents("php://input");

        // convert object to array
        $receive = json_decode($content, true);

        // take table name from the array
        $table = $receive['table'];

        // take data set from the array
        $data = $receive['data'];

        // set a default condition
        $condition = array();

        // check the condition exists into the array
        if(array_key_exists('cond', $receive)){
            $condition = $receive['cond'];
        }

        // get information from database
        $result = $this->action->update($table, $data, $condition);

        // convert the information array to JSON string
        echo json_encode($result);

    }

}