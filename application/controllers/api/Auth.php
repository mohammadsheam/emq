<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'libraries/REST_Controller.php';

class Auth extends REST_Controller{
    function __construct(){
        parent::__construct();
        // load necessary files
        $this->load->model('action');
        $this->load->helper("security");
        $this->load->helper("methods");
        $this->load->library('encryption');
        $this->load->library("form_validation");

        // check API KEY Matched
        $headers = getallheaders();
        $this->apiKey = $headers['X-API-KEY'];

    }


    // GET: <project_url>/api/Auth
    public function login_post(){

        if(auth_key($this->apiKey)){

            $mobile  = $this->input->post('username');
            $password  = $this->input->post('password');

            $this->form_validation->set_rules("username", "Username", "required|xss_clean");
            $this->form_validation->set_rules("password", "Password", "required|xss_clean");

            if ($this->form_validation->run() == TRUE){

                $where = array('trash' => 0,'mobile' => $mobile);
                $data = $this->action->read('clients', $where);

                if(count($data) > 0){

                    if($this->encryption->decrypt($data[0]->password) == $password){
                        return $this->response(array(
                            "status"  => 1,
                            "message" => "Data found",
                            "data"    => $data
                        ), REST_Controller::HTTP_OK);
                    }else{
                        return $this->response(array(
                            "status"  => 0,
                            "message" => "Password Not Matched",
                        ), REST_Controller::HTTP_OK);
                    }

                }else{
                    return $this->response(array(
                        "status"  => 0,
                        "message" => "Username name not found",
                        "data" => $where,
                    ), REST_Controller::HTTP_NOT_FOUND);
                }

            }else{
                return $this->response(array(
                    "message" => "Error: ".validation_errors(),
                ), REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else{
            return $this->response(array(
                    "message" => "API key not matched!!",
                ), REST_Controller::HTTP_UNAUTHORIZED);
        }
    }


    //POST: <project_url>/api/Auth
    public function registration_post(){
        
        if(auth_key($this->apiKey)){

            $name      = $this->input->post('name');
            $mobile    = $this->input->post('mobile');
            $password  = $this->input->post('password');
            $address   = !empty($this->input->post('address'))? $this->input->post('address') : '';
            
            // form validation for inputs
            $this->form_validation->set_rules("name", "Name", "required|xss_clean");
            $this->form_validation->set_rules("password", "Password", "required|xss_clean");
            $this->form_validation->set_rules("mobile", "Mobile", "required|is_unique[clients.mobile]|xss_clean");
            if (empty($_FILES['image_file']['name']))
            {
                $this->form_validation->set_rules('image_file', 'Image', 'required');
            }

            if ($this->form_validation->run() == TRUE){

                $data = array(
                    'date'      => date('Y-m-d'),
                    'name'      => $name,
                    'mobile'    => $mobile,
                    'client_id' => generate_id('clients'),
                    'password'  => $this->encryption->encrypt($password),
                    'address'   => $address,
                    'image'     => uploadImage(),
                );

                $status = $this->action->add('clients', $data);

                if($status){
                    $this->response(array(
                        "status" => 1,
                        "message" => "Data Inserted Successfully",
                    ), REST_Controller::HTTP_OK);
                }else{
                    $this->response(array(
                        "status" => 0,
                        "message" => "Data not inserted",
                    ), REST_Controller::HTTP_NOT_FOUND);
                }
                
            }else{
                return $this->response(array(
                    "message" => "Error: ".validation_errors(),
                ), REST_Controller::HTTP_UNAUTHORIZED);
            }

        }else{
            return $this->response(array(
                "message" => "API key not matched!!",
            ), REST_Controller::HTTP_UNAUTHORIZED);
        }

    }

}