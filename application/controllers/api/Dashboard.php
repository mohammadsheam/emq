<?php

require APPPATH.'libraries/REST_Controller.php';

class Dashboard extends REST_Controller {
    function __construct() {
        parent::__construct();
        $this->load->helper('ajax');
    }

    public function index_get()
    {
        $client_id = $this->input->get('client_id');

        if (!empty($client_id)) {

            // current Balance
            $current_b = currentBalance($client_id);

            // totall Order
            $totall_order = totallOrder($client_id);

            // pending order
            $pending_order = pendingOrder($client_id);

            // make data array
            $data = array(
                'balance' => $current_b,
                'totall_order' => $totall_order,
                'pending_order' => $pending_order,
            );

            $this->response(array(
                'status' => 1,
                'message' => 'Data Found',
                'data' => $data
            ), REST_Controller::HTTP_OK);

        }else{
            $this->response(array(
                'status' => 0,
                'message' => 'Client Id not provided',
            ), REST_Controller::HTTP_NOT_FOUND);
        }

    }

}