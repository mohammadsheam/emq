<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends Backend {
	
	function __construct(){
		parent::__construct();
	}

	public function index(){
		$this->data['title'] = lang('Admin_panel');
		$this->data['notification'] = null;
		
		$this->load_page('admin/dashboard');
	}

}


/* End of file Dashboard.php */
/* Location: ./application/controllers/admin/Dashboard.php */
