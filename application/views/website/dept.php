<main id="main">
  <section id="post_section" class="post_section">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 d-flex">
            <!-- call left sidebar here -->
        </div>

        <div class="col-lg-9 col-md-7 col-sm-6 col-xs-12  right_uploadedcontains">
          <div class="row">
            <div class="div_title">
              <h3>প্রশাসন বিভাগ</h3>
            </div>

          </div>
          <div class="row content_start">

            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="title"><a href="">সভাপতিগন</a></h4>
                  <p class="description">আমাদের সকল বিভাগ সমূহ এখানে দেখতে পাবেন। </p>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="title"><a href="">মহাসচিবগণ</a></h4>
                  <p class="description">আমাদের সকল বিভাগ সমূহ এখানে দেখতে পাবেন। </p>
                </div>
              </div>
            </div>

            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="title"><a href="">পরীক্ষা-নিয়ন্ত্রক</a></h4>
                  <p class="description">আমাদের সকল বিভাগ সমূহ এখানে দেখতে পাবেন। </p>
                </div>
              </div>
            </div>


            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="title"><a href="">স্টাফগণ</a></h4>
                  <p class="description">আমাদের সকল বিভাগ সমূহ এখানে দেখতে পাবেন। </p>
                </div>
              </div>
            </div>


            <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
              <div class="card">
                <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                <div class="card-body">
                  <h4 class="title"><a href="">যোগাযোগ</a></h4>
                  <p class="description">আমাদের সকল বিভাগ সমূহ এখানে দেখতে পাবেন। </p>
                </div>
              </div>
            </div>

          </div>

        </div>

      </div>

    </div>
  </section>
  
</main><!-- End #main -->
