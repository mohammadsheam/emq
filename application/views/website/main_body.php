<main id="main">
   <section id="notice">
      <div class="container-fluid">
         <div class="custom">
            <div class="col-lg-12">
               <div id="client-testimonial-carousel" class="carousel slide" data-ride="carousel">
                  <div class="carousel-inner" role="listbox">
                     <div class="carousel-item active text-center p-4">
                        <blockquote class="blockquote text-center">
                           <p class="mb-0"> ৪৪তম কেন্দ্রীয় পরীক্ষার শিক্ষার্থী নিবন্ধন ফরম
                           </p>
                           
                        </blockquote>
                     </div>
                     <div class="carousel-item text-center p-4">
                        <blockquote class="blockquote text-center">
                           <p class="mb-0"> ফযীলত থেকে ইবতেদাইয়্যাহ মারহালা পর্যন্ত ৪৩তম কেন্দ্রীয় পরীক্ষার ৫০% ফি ফেরত সংক্রান্ত বিজ্ঞপ্তি
                           </p>
                           
                        </blockquote>
                     </div>
                     <div class="carousel-item text-center p-4">
                        <blockquote class="blockquote text-center">
                           <p class="mb-0"> কওমী মাদরাসা শিক্ষার পর্যায় ও মারহালা (স্তর) সমূহের বিবরণ
                           </p>
                        </blockquote>
                     </div>
                     <ol class="carousel-indicators">
                        <li data-target="#client-testimonial-carousel" data-slide-to="0" class="active"></li>
                        <li data-target="#client-testimonial-carousel" data-slide-to="1"></li>
                        <li data-target="#client-testimonial-carousel" data-slide-to="2"></li>
                     </ol>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- post section start -->
   <section id="post_section" class="post_section">
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 d-flex" id="sidebar">
               <!-- call left sidebar here -->

            </div>
            <div class="col-lg-9 col-md-7 col-sm-6 col-xs-12 ">
                  <div class="col-md-12 div_title">
                     <h3><span>আপডেট</span> <span class="pull-right"><a href="all_post.html" target="__blank">সকল  >>></a></span></h3>
                  </div>
               
               <div class="row content_start">
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক ফি দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="<?php echo base_url('public/') ?>assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <img src="assets/img/body_upload/madrasa_620.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                           <h5 class="card-title">মারহালা পরিবর্তন ফি</h5>
                           <p class="card-text">(ক) মারহালা পরিবর্তনের ক্ষেত্রে : যে মারহালায় উন্নীত হবে, ঐ মারহালার ইলহাকী ফি-এর অর্ধেক
                              ফি
                              দিতে হবে। আর মাঝে ..</p>
                           <a href="single_post.html" class="btn btn-success">আরও পড়ুন</a>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- post section end -->
   <br><br>
   <!-- ======= Featured Services Section ======= -->
   <section id="featured-services" class="featured-services">
      <div class="container">
         <div class="row">
            <div class="col-lg-6 col-md-6">
               <a href="all_departments.html" target="__blank">
               <div class="icon-box">
                  <div class="icon"><i class="icofont-ui-office"></i></div>
                  <h4 class="title">সকল বিভাগ</h4>
                  <p class="description">আমাদের সকল বিভাগ সমূহ এখানে দেখতে পাবেন। </p>
               </div>
               </a>
            </div>
            <div class="col-lg-6 col-md-6 mt-4 mt-md-0">
               <a href="#portfolio">
               <div class="icon-box">
                  <div class="icon"><i class="icofont-ui-image"></i></div>
                  <h4 class="title">গ্যালারি</h4>
                  <p class="description">আমাদের কাজকর্মের কিছু চিত্র </p>
               </div>
               </a>
            </div>
         </div>
      </div>
   </section>
   <!-- End Featured Services Section -->
   <br><br>
   <!-- ======= About Section ======= -->
   <section id="document" class="document section-bg">
      <div class="container-fluid">
         <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12 d-flex">
               <div class="left_link">
                  <div class="sticky-top">
                     <div class="div_title">
                        <h5>সর্বাধিক গুরুত্বপূর্ণ ফাইল সমূহ</h5>
                     </div>
                     <ul>
                        <li><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf" target="_blank"><i class="fas fa-cloud-download-alt"></i>&nbsp;(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</a></li>
                        <li><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf" target="_blank"><i class="fas fa-cloud-download-alt"></i>&nbsp;(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</a></li>
                        <li><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf" target="_blank"><i class="fas fa-cloud-download-alt"></i>&nbsp;(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</a></li>
                     </ul>
                     
                  </div>
               </div>
            </div>
            
            <div class="col-lg-9 col-md-7 col-sm-6 col-xs-12 ">

               <div class="col-md-12 div_title">
                  <h3><span>ডকুমেন্টসমূহ </span> <span class="pull-right"><a href="all_post.html" target="__blank">সকল >>></a></span></h3>
               </div>
               <div class="row content_start">
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <div class="container">
                           <img src="<?php echo base_url('public/') ?>assets/documents/forms.svg" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body">
                           <h5 class="card-title">(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</h5>
                           <button class="btn btn-dark"><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf"><i class="fa fa-download" aria-hidden="true"></i> </a></button>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <div class="container">
                           <img src="<?php echo base_url('public/') ?>assets/documents/forms.svg" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body">
                           <h5 class="card-title">(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</h5>
                           <button class="btn btn-dark"><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf"><i class="fa fa-download"
                                    aria-hidden="true"></i> </a></button>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <div class="container">
                           <img src="<?php echo base_url('public/') ?>assets/documents/forms.svg" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body">
                           <h5 class="card-title">(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</h5>
                           <button class="btn btn-dark"><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf"><i class="fa fa-download"
                                    aria-hidden="true"></i> </a></button>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <div class="container">
                           <img src="<?php echo base_url('public/') ?>assets/documents/forms.svg" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body">
                           <h5 class="card-title">(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</h5>
                           <button class="btn btn-dark"><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf"><i class="fa fa-download"
                                    aria-hidden="true"></i> </a></button>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <div class="container">
                           <img src="<?php echo base_url('public/') ?>assets/documents/forms.svg" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body">
                           <h5 class="card-title">(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</h5>
                           <button class="btn btn-dark"><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf"><i class="fa fa-download"
                                    aria-hidden="true"></i> </a></button>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-3 col-md-6 col-sm-12 col-xs-12">
                     <div class="card">
                        <div class="container">
                           <img src="<?php echo base_url('public/') ?>assets/documents/forms.svg" class="card-img-top" alt="...">
                        </div>
                        <div class="card-body">
                           <h5 class="card-title">(পুরুষ) মাদ্রাসা বেফাকভুক্তির আবেদন ফরম</h5>
                           <button class="btn btn-dark"><a href="assets/documents/ইলহাকী-ফরম-পুরুষ.pdf"><i class="fa fa-download"
                                    aria-hidden="true"></i> </a></button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>

         </div>
      </div>
   </section>
   <!-- End Featured Services Section -->
   <br><br>

   <!-- ======= About Section ======= -->
   <section id="about" class="about">
      <div class="container">
         <div class="row">
            <!--left side-->
            <div class="col-lg-6">
               <img src="<?php echo base_url('public/') ?>assets/img/about.jpg" class="img-fluid" alt="">
            </div>
            <!--right side-->
            <div class="col-lg-6 pt-4 pt-lg-0 content">
               <h3>আমাদের-সম্পর্কে</h3>
               <p class="font-italic">
                  আহলুস সুন্নাত ওয়াল জামায়াতের চিন্তাধারা এবং দারুল উলূম দেওবন্দের অনুসরণীয় পথই হবে
                  কওমী মাদরাসাসমূহের এবং ইত্তেফাকুল মাদারিসিল ক্বাওমিয়া, ময়মনসিংহ-এর একমাত্র পথ ও আদর্শ।
               </p>
               <ul>
                  <li><i class="icofont-check-circled"></i>  কওমী মাদরাসা সমূহকে এক প্লাটফরমে ঐক্যবদ্ধ করা।</li>
                  <li><i class="icofont-check-circled"></i>  তালীম ও তারবিয়াতের মান উন্নয়ন।</li>
                  <li><i class="icofont-check-circled"></i>  সমাজের দ্বীনী চাহিদা পূরণ।</li>
                  <li><i class="icofont-check-circled"></i>  ইসলামের হিফাযত, প্রচার, প্রসার, দাওয়াত ও তাবলীগ এবং ইসলামের উপর আবর্তিত যে কোন হামলার সুদৃঢ় ও প্রামাণ্য জবাব দান।</li>
                  <li><i class="icofont-check-circled"></i>  সমাজে উলামায়ে কিরামের ন্যায়সঙ্গত অধিকার ও মর্যাদাপূর্ণ অবস্থান প্রতিষ্ঠা।</li>
               </ul>
            </div>
         </div>
      </div>
   </section>
   <!-- End About Section -->
   <!-- ======= Counts Section ======= -->
   <section id="counts" class="counts section-bg">
      <div class="container">
         <div class="row counters">
            <div class="col-lg-3 col-6 text-center">
               <span data-toggle="counter-up">232</span>
               <p>মাদ্রাসা</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
               <span data-toggle="counter-up">21</span>
               <p>মারকাজ</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
               <span data-toggle="counter-up">1,463</span>
               <p>ছাত্র-ছাত্রী</p>
            </div>
            <div class="col-lg-3 col-6 text-center">
               <span data-toggle="counter-up">105</span>
               <p>বিভাগ</p>
            </div>
         </div>
      </div>
   </section>
   
   <!-- ======= Portfolio Section ======= -->
   <section id="portfolio" class="portfolio">
      <div class="container">
         <div class="section-title">
            <span>ছবি গ্যালারি</span>
            <h2>ছবি গ্যালারি</h2>
            <p>আমাদের কাজকর্মের কিছু অংশ </p>
         </div>
         <div class="row">
            <div class="col-lg-12 d-flex justify-content-center">
               <ul id="portfolio-flters">
                  <li data-filter="*" class="filter-active">সকল</li>
                  <li data-filter=".filter-app">বাৎসারিক মাহফিল</li>
                  <li data-filter=".filter-card">Card</li>
                  <li data-filter=".filter-web">Web</li>
               </ul>
            </div>
         </div>
         <div class="row portfolio-container">
            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>১</h4>
                  <p>বাৎসারিক মাহফিল</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-1.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 1"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>Web 3</h4>
                  <p>Web</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-2.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>২</h4>
                  <p>বাৎসারিক মাহফিল</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-3.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 2"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>Card 2</h4>
                  <p>Card</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-4.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 2"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>Web 2</h4>
                  <p>Web</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-5.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 2"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-app">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>৩</h4>
                  <p>বাৎসারিক মাহফিল</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-6.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="App 3"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-7.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>Card 1</h4>
                  <p>Card</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-7.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 1"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-card">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-8.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>Card 3</h4>
                  <p>Card</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-8.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Card 3"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
            <div class="col-lg-4 col-md-6 portfolio-item filter-web">
               <img src="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-9.jpg" class="img-fluid" alt="">
               <div class="portfolio-info">
                  <h4>Web 3</h4>
                  <p>Web</p>
                  <a href="<?php echo base_url('public/') ?>assets/img/portfolio/portfolio-9.jpg" data-gall="portfolioGallery" class="venobox preview-link" title="Web 3"><i class="bx bx-plus"></i></a>
                  <a href="portfolio-details.html" class="details-link" title="More Details"><i class="bx bx-link"></i></a>
               </div>
            </div>
         </div>
      </div>
   </section>
  
   <!-- ======= Team Section ======= -->
   <section id="team" class="team section-bg">
      <div class="container">
         <div class="section-title">
            <span>উচ্চপদস্থ কর্মকর্তাগণ</span>
            <h2>উচ্চপদস্থ কর্মকর্তাগণ</h2>
            <p>উচ্চপদস্থ কর্মকর্তাগণ - এর একাংশ</p>
         </div>
         <div class="row">
            
            <div id="testimonial-slider" class="owl-carousel">
              <div class="testimonial">
               <div class="testimonial-content">
                  <div class="pic">             
                     <img src="<?php echo base_url('public/') ?>assets/img/team/team-1.jpg"/>
                  </div>
                  <h3 class="name">মাওলানা মাহফুযুল হক</h3>
                  <span class="title">মহাসচিব (ভারপ্রাপ্ত)</span>
               </div>
              </div>
              <div class="testimonial">
               <div class="testimonial-content">
                  <div class="pic">
                    <img src="<?php echo base_url('public/') ?>assets/img/team/team-2.jpg"/>
                  </div>
                  <h3 class="name">হযরত মাওলানা নূর হোসাইন কাসেমী</h3>
                  <span class="title">সহ: সভাপতি</span>
                </div>                      
              </div>
              <div class="testimonial">
                <div class="testimonial-content">
                  <div class="pic">
                   <img src="<?php echo base_url('public/') ?>assets/img/team/team-3.jpg"/>
                  </div>
                  <h3 class="name">হযরত মাওলানা মুফতী মুহাম্মদ ওয়াক্কাস</h3>
                  <span class="title">সহ: সভাপতি</span>
                </div>
                <!-- <p class="description">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p> -->
                
              </div>
              <div class="testimonial">
                <div class="testimonial-content">
                  <div class="pic">
                   <img src="<?php echo base_url('public/') ?>assets/img/team/team-1.jpg"/>
                  </div>
                  <h3 class="name">হযরত মাওলানা সালাহ উদ্দীন</h3>
                  <span class="title">সহ: সভাপতি</span>
                </div>
                <!-- <p class="description">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p> -->
               
              </div>
              <div class="testimonial">
                <div class="testimonial-content">
                  <div class="pic">
                    <img src="<?php echo base_url('public/') ?>assets/img/team/team-2.jpg"/>
                  </div>
                  <h3 class="name">হযরত মাওলানা নূর হোসাইন কাসেমী</h3>
                  <span class="title">সহ: সভাপতি</span>
                </div>
                  <!-- <p class="description">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p> -->
               
              </div>
                      <div class="testimonial">
                <div class="testimonial-content">
                  <div class="pic">
                    <img src="<?php echo base_url('public/') ?>assets/img/team/team-3.jpg"/>
                  </div>
                  <h3 class="name">হযরত মাওলানা মুফতী মুহাম্মদ ওয়াক্কাস</h3>
                  <span class="title">সহ: সভাপতি</span>
                </div>
                <!-- <p class="description">
                 Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p> -->
               
              </div>
              <div class="testimonial">
               <div class="testimonial-content">
                  <div class="pic">             
                     <img src="<?php echo base_url('public/') ?>assets/img/team/team-1.jpg"/>
                  </div>
                  <h3 class="name">মাওলানা মাহফুযুল হক</h3>
                  <span class="title">মহাসচিব (ভারপ্রাপ্ত)</span>
                </div>
                <!-- <p class="description">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry. 
                </p> -->
                
              </div>
              <div class="testimonial">
               <div class="testimonial-content">
                  <div class="pic">
                    <img src="<?php echo base_url('public/') ?>assets/img/team/team-2.jpg"/>
                  </div>
                  <h3 class="name">হযরত মাওলানা নূর হোসাইন কাসেমী</h3>
                  <span class="title">সহ: সভাপতি</span>
                </div>
                <!-- <p class="description">
                  Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                </p> -->
                
              </div>
             
            </div>
           
         </div>
      </div>
   </section>
   <!-- End Team Section -->
   <!-- ======= Contact Section ======= -->
   <section id="contact" class="contact">
      <div class="container">
         <div class="section-title">
            <span>যোগাযোগ</span>
            <h2>যোগাযোগ</h2>
            <p class="text-justify">কাজের সুবিধার্থে ও সুষ্ঠভাবে পরিচালনার্থে ইত্তেফাকুল মাদারিসিল ক্বাওমিয়া, ময়মনসিংহ-এর কার্যক্রমগুলো কয়েকটি ভাগে ভাগ করা হয়েছে যেমন: প্রশাসন বিভাগ, তা’লিম তারবিয়্যাত বিভাগ, পরীক্ষা নিয়ন্ত্রণ বিভাগ, হিসাব বিভাগ ও প্রকাশনা বিভাগ। এছাড়াও কয়েকটি শাখা রয়েছে যেমন : রেজিস্ট্রেশন শাখা, আইটি শাখা, কুপন শাখা ও প্রশিক্ষণ শাখা । অতএব আপনাদের কোন বিষয়ে ইত্তেফাকুল মাদারিসিল ক্বাওমিয়া, ময়মনসিংহ-এর সাথে যোগাযোগের প্রয়োজন হলে প্রথমে আপনার বিষয়টি কী তা চিন্তা করতে হবে। তারপর উক্ত বিষয়ের জন্য নির্দিষ্ট বিভাগে ফোন করতে হবে। নিম্নে কোন কোন বিভাগে ফোন করবেন নাম্বারসহ বিস্তারিত তথ্য প্রদান করা হল।</p>
         </div>
         <div class="row">
            <div class="col-lg-5 d-flex align-items-stretch">
               <div class="info">
                  <table class="table table-hover">
                     <tr>
                        <th>বিভাগ</th>
                        <th>ফোন নাম্বার</th>
                     </tr>
                     <tr>
                        <td>আই. টি. বিভাগ</td>
                        <td>01822930055</td>
                     </tr>
                     <tr>
                        <td>পরীক্ষা বিভাগ</td>
                        <td>01877385946</td>
                     </tr>
                     <tr>
                        <td>রেজিস্ট্রেশন বিভাগ</td>
                        <td>01877385946</td>
                     </tr>
                     <tr>
                        <td>তা’লিম তরবিয়াত বিভাগ</td>
                        <td>01725837115</td>
                     </tr>
                     <tr>
                        <td>প্রকাশনা বিভাগ</td>
                        <td>01798288392</td>
                     </tr>
                  </table>
               </div>
            </div>
            <div class="col-lg-7 mt-5 mt-lg-0 d-flex align-items-stretch">
               <div class="info">
                  <div class="address">
                     <i class="icofont-google-map"></i>
                     <h4>লোকেশন:</h4>
                     <p>A108 Adam Street, New York, NY 535022</p>
                  </div>
                  <div class="email">
                     <i class="icofont-envelope"></i>
                     <h4>ইমেইলঃ</h4>
                     <p>info@example.com</p>
                  </div>
                  <div class="phone">
                     <i class="icofont-phone"></i>
                     <h4>ফোনঃ</h4>
                     <p>+1 5589 55488 55s</p>
                  </div>
                  <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d12097.433213460943!2d-74.0062269!3d40.7101282!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb89d1fe6bc499443!2sDowntown+Conference+Center!5e0!3m2!1smk!2sbg!4v1539943755621" frameborder="0" style="border:0; width: 100%; height: 290px;" allowfullscreen></iframe>
               </div>
            </div>
         </div>
      </div>
   </section>
   <!-- End Contact Section -->
</main>
<!-- End #main -->