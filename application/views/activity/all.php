<div class="container-fluid">
    <!--read user name-->
    <?php 
        if($this->input->post('user_id')){
            $where = array('id' => $this->input->post('user_id'));
        }else{
            $where = array('id' => $this->session->userdata('user_id'));
        } 
        $info = $this->action->read('users', $where);
    ?>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="float-left">Search
                        <span class="badge badge-success animated zoomIn slow"><?php echo ($info)? $info[0]->fullname:'BloodLust D';?></span>
                    </h3>
                    <a class="collapsed btn btn-primary float-right m-t-0" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        <i class="m-r-5 fas fa-search-plus" aria-hidden="true"></i>
                    </a>
                </div>
                <div id="collapseOne" class="collapse">
                    <div class="card-body">
                        <?php echo form_open(); ?>
                        <div class="form-group row">
                            <label class="col-sm-3 text-right"> <?php echo lang('Form') ;?> </label>
                            <div class="input-group col-sm-6">
                                <input type="text" class="form-control" placeholder="Y-m-d" name="search[dateFrom]" id="datepicker-autoclose" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 text-right"> <?php echo lang('To') ;?></label>
                            <div class="input-group col-sm-6">
                                <input type="text" class="form-control" placeholder="Y-m-d" name="search[dateTo]" id="datepicker-autoclose1" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 text-right">Select User</label>
                            <div class="col-sm-6">
                                <select class="select2 form-control" name="user_id" >
                                    <option selected disabled> &nbsp;</option>
                                    <?php foreach ($users as $value) {?>
                                    <option value="<?php echo $value->id;?>"><?php echo $value->fullname;?></option>
                                    <?php } ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="" class="col-sm-8">&nbsp;</label>
                            <div class="col-sm-3">
                                <input type="submit" class="btn btn-success" name="submit" value="search">
                            </div>
                        </div>
                        <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="float-left">
                        Action Activity
                    </h3>

                    <a class="collapsed btn btn-primary float-right m-t-0" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
                        <i class="m-r-5 fas fa-minus" aria-hidden="true"></i>
                    </a>
                </div>

                <div class="collapse show card-body" id="collapseTwo">
                    <table class="table table-bordered" id="data-table">
                        <thead class="thead-dark">
                        <tr>
                            <th>SL</th>
                            <th><?php echo lang('Date');?></th>
                            <th>Module</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($actions as $key => $action){?>
                        <tr>
                            <td><?php echo $key+1;?></td>
                            <td><?php echo $action->date;?></td>
                            <td><?php echo $action->module;?></td>
                            <td><?php echo $action->action;?></td>
                        </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">&nbsp;</div>
            </div>
        </div>
    </div>


    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="float-left">
                        Access Info
                    </h3>

                    <a class="collapsed btn btn-primary float-right m-t-0" data-toggle="collapse" data-target="#collapseThree" aria-expanded="true" aria-controls="collapseThree">
                        <i class="m-r-5 fas fa-minus" aria-hidden="true"></i>
                    </a>
                </div>

                <div class="collapse show card-body" id="collapseThree">
                    <table class="table table-bordered" id="data_table2">
                        <thead class="thead-dark">
                        <tr>
                            <th>SL</th>
                            <th>Browser</th>
                            <th>Login</th>
                            <th>Logout</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($logs as $key => $log){?>
                            <tr>
                                <td><?php echo $key+1;?></td>
                                <td><?php echo $log->browser;?></td>
                                <td><?php echo $log->login_time;?></td>
                                <td><?php echo $log->logout_time;?></td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer">&nbsp;</div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#data_table2').DataTable();
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });

    jQuery('#datepicker-autoclose1').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'yyyy-mm-dd'
    });
</script>
