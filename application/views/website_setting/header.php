<?php $info = json_decode($record[0]->header); ?>
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-md-9">
		<?php echo form_open_multipart('', ''); ?>

			<input type="hidden" name="old_logo" value="<?php echo $info->logo; ?>">
			<input type="hidden" name="old_fav" value="<?php echo $info->favicon; ?>">

			<div class="card">
				<div class="card-header">
				    <h3 class="card-title m-b-0"><?php echo lang('Header_Setting') ;?></h3>
                </div>
				<div class="card-body">
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Site_Name') ;?> <span class="req">*</span></label>
					    <div class="col-sm-6">
					        <input type="text" name="site_title" class="form-control" value="<?php echo $info->site_title; ?>" required>
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Change_Logo') ;?> </label>
					    <div class="col-sm-6">
					        <input type="file" name="logo" class="form-control">
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Change_Favicon') ;?></label>
					    <div class="col-sm-6">
					        <input type="file" name="favicon" class="form-control" >
					    </div>
					</div>
				</div>
				<div class="card-footer">
					<input type="submit" name="submit" class="btn btn-primary float-right" value="<?php echo lang('save');?>">
				</div>
			</div>
		<?php echo form_close();?>
		</div>
		<div class="col-md-3">
			<p>
				<img class="animated slideInRight slow" title="Logo" src="<?php echo base_url('').$info->logo;?>" alt="Logo" width="150" height="150">
			</p>
			<p>
				<img class="animated slideInRight slow image" src="<?php echo base_url('').$info->favicon;?>" alt="Favicon" width="100" height="100">
			</p>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
