<?php $info = json_decode($site_info[0]->header); ?>

<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta content="width=device-width, initial-scale=1.0" name="viewport">
      <title><?php echo $info->site_title; ?></title>
      <meta content="" name="descriptison">
      <meta content="" name="keywords">
      <!-- Favicons -->
      <link href="<?php echo base_url(''.$info->favicon) ?>" rel="icon">
      <!-- <link href="<?php //echo base_url('public/') ?>assets/img/apple-touch-icon.png" rel="apple-touch-icon"> -->
      <!-- Google Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">
      <!-- font awesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
      <!-- Vendor CSS Files -->
      <link href="<?php echo base_url('public/') ?>assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <link href="<?php echo base_url('public/') ?>assets/vendor/icofont/icofont.min.css" rel="stylesheet">
      <link href="<?php echo base_url('public/') ?>assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
      <link href="<?php echo base_url('public/') ?>assets/vendor/venobox/venobox.css" rel="stylesheet">
      <link href="<?php echo base_url('public/') ?>assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
      <!-- Template Main CSS File -->
      <link href="<?php echo base_url('public/') ?>assets/css/style.css" rel="stylesheet">
      <!-- slider script -->
      <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/') ?>assets/css/slider.style.css" />
      <!-- <script type="text/javascript" src="engine1/jquery.js"></script> -->
      <script src="<?php echo base_url('public/') ?>assets/vendor/jquery/jquery.min.js"></script>
   	</head>
<body>
	<!-- preloader section -->
	<!-- <div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div> -->