<div class="left_link">
    <div class="sticky-top">
       <div class="div_title">
          <h5>প্রয়োজনীয় লিঙ্ক সমূহ</h5>
       </div>
       <ul>
          <li><a href="https://wifaqbd.org/" target="_blank"><i class="icofont-check-circled"></i>&nbsp;বেফাকুল মাদারিসিল আরাবিয়া বাংলাদেশ</a></li>
          <li><a href="https://alhaiatululya.com/" target="_blank"><i class="icofont-check-circled"></i>&nbsp;আল-হাইআতুল উলয়া লিল-জামি’আতিল কওমিয়া বাংলাদেশ</a></li>
          <li><a href="https://mora.gov.bd/" target="_blank"><i class="icofont-check-circled"></i>&nbsp;ধর্ম বিষয়ক মন্ত্রণালয়</a></li>
          <li><a href="https://www.jamiaashrafia.org/" target="_blank"><i class="icofont-check-circled"></i>&nbsp;জামিয়া আশরাফিয়া, লাহোর</a></li>
          <li><a href="http://www.bmeb.gov.bd/" target="_blank"><i class="icofont-check-circled"></i>&nbsp;বাংলাদেশ মাদ্রাসা শিক্ষা বোর্ড</a></li>
          <li><a href="http://www.mymensinghdiv.gov.bd/" target="_blank"><i class="icofont-check-circled"></i>&nbsp;ময়মনসিংহ বিভাগ</a></li>
       </ul>
       <div class="visitor_part">
          <div class="div_title">
             <h5>ভিজিটর</h5>
          </div>
          <ul>
             <li><a href="#">বর্তমান ভিজিটর: ১০</a> </li>
             <li><a href="#">আজকের মোট ভিজিটর: ১০০</a></li>
             <li><a href="#">সর্বমোট ভিজিটর: ১০০০</a></li>
          </ul>
       </div>
    </div>
 </div>