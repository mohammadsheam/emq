<!-- ======= Footer ======= -->
<footer id="footer">
 <div class="footer-top">
    <div class="container">
       <div class="row  justify-content-center">
          <div class="col-lg-6">
             <h3>ইত্তেফাকুল মাদারিসিল ক্বাওমিয়া, ময়মনসিংহ ।</h3>
             <p>একটি আদর্শ দ্বীনি প্রতিষ্ঠান।</p>
          </div>
       </div>
       <?php //echo base_url('public/') ?>
       <div class="social-links">
          <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
          <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
          <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
          <a href="#" class="google-plus"><i class="bx bxl-skype"></i></a>
          <a href="#" class="linkedin"><i class="bx bxl-linkedin"></i></a>
       </div>
    </div>
 </div>
 <div class="container footer-bottom clearfix">
    <div class="copyright">
       &copy; কপিরাইট <strong><span>ইত্তেফাকুল মাদারিসিল ক্বাওমিয়া, ময়মনসিংহ।</span></strong> সমস্ত অধিকার সংরক্ষিত
    </div>
    <div class="credits">
       সাইট উন্নয়নে <a href="http://mohammadsheam.pythonanywhere.com/" target="__blank">Mohammad Sheam</a>
    </div>
 </div>
</footer>
<!-- End Footer -->
<a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>
<!-- Vendor JS Files -->
<!-- font-awesome -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" integrity="sha512-F5QTlBqZlvuBEs9LQPqc1iZv2UMxcVXezbHzomzS6Df4MZMClge/8+gXrKw2fl5ysdk4rWjR0vKS7NNkfymaBQ==" crossorigin="anonymous"></script>

<script src="<?php echo base_url('public/') ?>assets/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/jquery.easing/jquery.easing.min.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/php-email-form/validate.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/waypoints/jquery.waypoints.min.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/counterup/counterup.min.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/venobox/venobox.min.js"></script>
<script src="<?php echo base_url('public/') ?>assets/vendor/owl.carousel/owl.carousel.min.js"></script>
<!-- Template Main JS File -->
<script src="<?php echo base_url('public/') ?>assets/js/main.js"></script>
<script>
 $(document).ready(function(){
 var $prev = $('.previousx');
 var $next = $('.nextx');
 var mode = "auto";
 $prev.on({
 click: function(e){
   e.preventDefault();
   mode = "manual";
   showPreviousImage();
 }
 });
 $next.on({
 click: function(e){
   e.preventDefault();
   mode = "manual";
   showNextImage();
   
 }
 });
 
 setInterval(function(){
 if(mode==="auto"){
   showNextImage();
 }
 },5000);
 
 function showNextImage(){
   var $actEl = $('.activx');
   var $nextEl = $actEl.next('.slidex');
   if($nextEl.length){
     $actEl.removeClass('activx');
     $nextEl.addClass('activx');
   }else{
     $actEl.removeClass('activx');
     $('.slidex:first-child').addClass('activx');
   }
 }
 
 function showPreviousImage(){
   var $actEl = $('.activx');
   var $prevEl = $actEl.prev('.slidex');
   if($prevEl.length){
     $actEl.removeClass('activx');
     $prevEl.addClass('activx');
   }else{
     $actEl.removeClass('activx');
     $('.slidex.last').addClass('activx');
   }
 }
 });

$('.owl-carousel').owlCarousel({
        autoplay: true,
        autoplayTimeout: 5000,
        navigation: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 3
            },
            1200: {
                items: 4
            }

        }
    })

</script>