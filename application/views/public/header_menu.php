<?php $info = json_decode($site_info[0]->header); ?>
<!-- ======= Header ======= -->
<header id="header" class="fixed-top">
 <div class="container d-flex align-items-center">
    <h3 class="logo mr-auto"><a href="<?php echo base_url(); ?>" class="logo mr-auto"><img src="<?php echo base_url(''.$info->logo) ?>" alt="EMQ-logo" class="img-fluid"></a></h3>
    <!-- Uncomment below if you prefer to use an image logo -->
    <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
    <nav class="nav-menu d-none d-lg-block">
       <ul>
          <li class="active"><a href="#header">হোম</a></li>
          <li><a href="#about">আমাদের-সম্পর্কে</a></li>
          <li><a href="#portfolio">ছবি গ্যালারি</a></li>
          <li><a href="#team">উচ্চপদস্থ কর্মকর্তাগণ</a></li>
          <li class="drop-down">
             <a href="">বিভাগ সমূহ</a>
             <ul>
                <?php foreach ($allDepts as $key => $value) { ?>
                <li><a href="<?php echo base_url('Home/department_view/'.$value->id); ?>"><?php echo $value->name; ?> </a></li>
                <?php } ?>
             </ul>
          </li>
          <li><a href="#contact">যোগাযোগ</a></li>
       </ul>
    </nav>
    <!-- .nav-menu -->
 </div>
</header>
<!-- End Header -->