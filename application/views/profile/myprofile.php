<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
					<p class="card-title m-b-0">
						<h3 class="float-left"></i><?php echo lang('Profile') ;?></h3>
						<a href="<?php echo site_url('profile/all');?>" class="btn btn-primary float-right"><i class="mdi mdi-keyboard-return"></i><?php echo lang('Back');?></a>
					</p>
                </div>
				<div class="card-body">
					<div class="row">
						<div class="col-md-4">
							<?php if($user_privilege == 'superadmin'){?>
							<img src="<?php echo base_url('private/assets/images/users/1-old.jpg');?>" class="rounded-circle animated bounceInUp slow" width="300" height="300">
						<?php }else{ ?>
							<img src="<?php echo base_url('').$info[0]->image;?>" class="rounded-circle animated bounceInUp slow" width="300" height="300">
						<?php } ?>
						</div>
						<div class="col-md-8">
							<table class="table table-bordered table-hover">
								<thead class="thead-dark animated zoomIn">
									<tr>
										<th class="text-center" colspan="2" >
											<?php echo lang('Information');?>
										</th>
									</tr>
								</thead>
								<tr>
									<th width="250" class="animated slideInLeft slow"><?php echo lang('Full_Name') ;?></th>
									<td class="animated slideInRight slow"><?php echo ($info)? $info[0]->fullname : $this->session->userdata('fullname');;?></td>
								</tr>
								<tr>
									<th width="250" class="animated slideInLeft slow"><?php echo lang('Username') ;?> </th>
									<td class="animated slideInRight slow"><?php echo ($info)? $info[0]->username: $this->session->userdata('username');;?></td>
								</tr>
								<tr>
									<th width="250" class="animated slideInLeft slow"><?php echo lang('Email') ;?> </th>
									<td class="animated slideInRight slow"><?php echo ($info)? $info[0]->email: '';?></td>
								</tr>
								<tr>
									<th width="250" class="animated slideInLeft slow"><?php echo lang('Privilege') ;?> </th>
									<td class="animated slideInRight slow"><?php echo ($info)? $info[0]->privilege: $this->session->userdata('privilege');;?></td>
								</tr>
								
							</table>
						</div>
					</div>
				</div>
				<div class="card-footer">
					&nbsp;
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
