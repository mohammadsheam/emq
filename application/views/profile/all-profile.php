<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header">
				    <h3 class="card-title m-b-0"><?php echo lang('All_Profile');?></h3>
                </div>
				<div class="card-body">
					<table class="table table-bordered table-hover" id="data-table">
						<thead class="thead-dark">
							<tr class="animated zoomIn slow">
								<th width="40px"><?php echo lang('SL') ;?></th>
								<th><?php echo lang('Image') ;?></th>
								<th><?php echo lang('Full_Name') ;?></th>
								<th><?php echo lang('Username') ;?></th>
								<th><?php echo lang('Email') ;?></th>
								<th><?php echo lang('Privilege') ;?></th>
								<th><?php echo lang('Action') ;?></th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($results as $key => $value) { ?>
							<tr class="animated slideInUp slow">
								<td><?php echo $key+1;?></td>
								<td><img src="<?php echo base_url('').$value->image;?>" alt="no image found" width="100"></td>
								<td><?php echo $value->fullname;?></td>
								<td><?php echo $value->username;?></td>
								<td><?php echo $value->email;?></td>
								<td><?php echo $value->privilege;?></td>
								<td>
									
									<?php if($user_privilege != 'superadmin' && permissionCheck('Profile', $userid, 'edit')){?>
									<a href="<?php echo site_url('profile/edit/').$value->id;?>"  class="btn btn-warning" title="Edit" ><i class="mdi mdi-table-edit" aria-hidden="true"></i></a>
									<a href="" class="btn btn-primary" 
										title="Change Password" 
										data-toggle="modal" 
										data-backdrop="static" 
										data-keyboard="false" 
										data-strict-close="true"
										data-target="#update"
										onclick="pass(<?php echo $value->id; ?>)"
										data-id="<?php echo $value->id; ?>"
										>
										<i class="mdi mdi-brightness-7" aria-hidden="true"></i>
									</a>
									<?php }else{ ?>
									<a href="<?php echo site_url('profile/edit/').$value->id;?>"  class="btn btn-warning" title="Edit" ><i class="mdi mdi-table-edit" aria-hidden="true"></i></a>
									<a href="" class="btn btn-primary" 
										title="Change Password" 
										data-toggle="modal" 
										data-backdrop="static" 
										data-keyboard="false" 
										data-strict-close="true"
										data-target="#update"
										onclick="pass(<?php echo $value->id; ?>)"
										data-id="<?php echo $value->id; ?>"
										>
										<i class="mdi mdi-brightness-7" aria-hidden="true"></i>
									</a>
									<?php } ?>

									<?php if($this->session->userdata('user_id') != $value->id ){?>

									<?php if($user_privilege != 'superadmin' &&  permissionCheck('profile', $userid, 'delete')){?>
									<a href="<?php echo site_url('profile/delete/').$value->id;?>" onclick="return confirm('Do you want to delete this Data?');" class="btn btn-danger" title="Delete" ><i class="mdi mdi-delete" aria-hidden="true"></i></a>
									<?php }else{ ?>
									<a href="<?php echo site_url('profile/delete/').$value->id;?>" onclick="return confirm('Do you want to delete this Data?');" class="btn btn-danger" title="Delete" ><i class="mdi mdi-delete" aria-hidden="true"></i></a>
									<?php } ?>	
									
									<?php } ?>
								</td>
							</tr>
							<?php }?>
						</tbody>
					</table>
				</div>
				<div class="card-footer">
					<h3>&nbsp;</h3>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

<!-- Update Info -->
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog  modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"><?php echo lang('Update')." ".lang('Password') ;?></h5>
            </div>
            <div class="modal-body">
                <?php echo form_open();?>

                <input type="hidden" name="id" class="oldId form-control">

                <div class="form-group row">
                    <label class="col-sm-4 text-right"><?php echo lang('New_Password') ;?></label>
                    <div class="col-sm-6">
                        <input type="password" name="new_pass" id="pass" placeholder="At least Six characters" minlength="6" class="form-control" required >
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-4 text-right"><?php echo lang('Confirm_Password') ;?></label>
                    <div class="col-sm-6">
                        <input type="password" id="con" class="form-control" required onkeyup='validation();' >
                        <span class="animated slideInUp slow" id="message"></span>
                    </div>
                </div>
                
            </div>
            <div class="modal-footer">
                <input type="reset" class="btn btn-secondary" data-dismiss="modal" value="Close">
                <input type="submit" id="myBtn" class="btn btn-primary" value="<?php echo lang('save');?>" name="update">
            </div>
            <?php echo form_close(); ?>
        </div>
    </div>
</div>

<script>
    function pass(id) {
	    var id = id;
	    $(".oldId").val(id);
	};

	function validation(){
		if (document.getElementById('pass').value ==
		   document.getElementById('con').value) {
		   	document.getElementById('myBtn').disabled = false;
		   document.getElementById('message').style.color = 'green';
		   document.getElementById('message').innerHTML = 'Matched!';
		 } else {
		   document.getElementById('myBtn').disabled = true;
		   document.getElementById('message').style.color = 'red';
		   document.getElementById('message').innerHTML = 'Not Matched';
		 }
	}

</script>