<style type="text/css">
	.req {
		color: red;
		font-weight: bold;
	}

	.op {
		color: gray;
		font-weight: lighter;
	}

</style>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-md-12">
		<?php echo form_open_multipart('', ''); ?>
			<div class="card">
				<div class="card-header">
				    <h3 class="card-title m-b-0"><?php echo lang('Create_Profile') ;?></h3>
                </div>
				<div class="card-body">
					<div class="form-group row">
					    <label class="col-sm-3 text-right"> <?php echo lang('Date') ;?> <span class="req">*</span></label>
					    <div class="input-group col-sm-6">
					        <input type="text" class="form-control" placeholder="YYYY-mm-dddd" name="date" id="datepicker-autoclose" required autocomplete="off">
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Image') ;?>  <span class="req">*</span></label>
					    <div class="col-sm-6">
					        <input type="file" name="image_file" class="form-control" required>
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Full_Name') ;?> <span class="req">*</span></label>
					    <div class="col-sm-6">
					        <input type="text" name="fullname" class="form-control" placeholder="Full Name" required>
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Username') ;?> <span class="req">*</span></label>
					    <div class="col-sm-6">
					        <input type="text" name="username" class="form-control" placeholder="User name" required>
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Email') ;?> <span class="op">[optional]</span></label>
					    <div class="col-sm-6">
					        <input type="email" name="email" class="form-control">
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Password');?> <span class="req">*</span></label>
					    <div class="col-sm-6">
					        <input type="text" name="pass" class="form-control" placeholder="At least 6 characters" min="6" required>
					    </div>
					</div>
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('Privilege') ;?> <span class="req">*</span></label>
					    <div class="col-sm-6">
					        <select class="select2 form-control" name="privilege" required >
					        	<option selected disabled> Select Privilege</option>
					        	<option value="admin">Admin</option>
					        	<option value="user">User</option>
					        </select>
					    </div>
					</div>

				</div>
				<div class="card-footer">
					<input type="submit" name="submit" class="btn btn-primary float-right" value="<?php echo lang('save');?>">
				</div>
			</div>
		<?php echo form_close();?>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
