<script type="text/javascript" src="<?php echo base_url('private/script/ngScript/developerSettingCtrl.js?'.time());?>"></script>
<style type="text/css">
	.req {
		color: red;
		font-weight: bold;
	}

	.op {
		color: gray;
		font-weight: lighter;
	}

</style>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid" ng-controller="developerSettingCtrl" ng-cloak>
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-md-12">
		<?php echo form_open('', ''); ?>
			<div class="card">
				<div class="card-header">
				    <h3 class="card-title m-b-0"><?php echo lang('add');?></h3>
                </div>
				<div class="card-body">
					<div class="form-group row">
					    <div class="col-sm-3">
					        <input type="text" name="controller" placeholder="controller name" class="form-control" required>
					    </div>
					    <div class="col-sm-3">
					        <input type="text" name="bncontroller" placeholder="Bangla name" class="form-control" required>
					    </div>
					</div>

					<div class="form-group row">
					    <div class="col-sm-6">
					        <input type="text" name="table" placeholder="Table name" class="form-control" required>
					    </div>
					</div>

					<div ng-repeat="item in list">
						<div class="form-group row">
							
						    <div class="col-sm-2">
						        <input type="text" name="colname[]" placeholder="Cloumn name" class="form-control" required>
						    </div>
						    <div class="col-sm-2">
						        <input type="text" name="colbname[]" placeholder="Cloumn bangla name" class="form-control" required>
						    </div>
						    <div class="col-sm-2">
						        <select class="form-control" name="coltype[]" required >
						        	<option selected disabled> Select column Type</option>
						        	<?php foreach (config_item('col_types') as $key => $value) { ?>
						        	<option value="<?php echo $key;?> " ><?php echo ucfirst($value);?></option>
						        	<?php } ?>
						        </select>
						    </div>
						    <div class="col-sm-2">
						        <input type="number" name="colsize[]" placeholder="Column size" class="form-control" required>
						    </div>
						    <div class="col-sm-2">
						        <select class="form-control" name="field_type[]" required >
						        	<option selected disabled> Select filed Type</option>
						        	<?php foreach (config_item('field_types') as $key => $value) { ?>
						        	<option value="<?php echo $key;?> " ><?php echo ucfirst($value);?></option>
						        	<?php } ?>
						        </select>
						    </div>

						    <div class="col-sm-2">
						    	<a class="btn btn-info" ng-click="addToList()" style="margin-right: 5px;" ><i class="fa fa-plus"></i></a>
	                            <a ng-show="$index != 0" ng-click="removeFromList($index)" class="btn btn-danger"><i class="fa fa-times"></i></a>
						    </div>
					    </div>
					</div>
				</div>
				<div class="card-footer">
					<input type="submit" name="submit" class="btn btn-primary float-right" value="<?php echo lang('save');?>">
				</div>
			</div>
		<?php echo form_close();?>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
