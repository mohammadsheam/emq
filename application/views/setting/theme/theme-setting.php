<style type="text/css">
	.req {
		color: red;
		font-weight: bold;
	}

	.op {
		color: gray;
		font-weight: lighter;
	}

</style>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-md-12">
		<?php echo form_open('', ''); ?>
			<div class="card">
				<div class="card-header">
				    <h3 class="card-title m-b-0"><?php echo lang('theme_setting');?></h3>
                </div>
				<div class="card-body">
					<div class="form-group row">
					    <label class="col-sm-3 text-right"><?php echo lang('change_Language');?><span class="req"> *</span></label>
					    <div class="col-sm-6">
					        <div class="col-sm-6">
						        <select class="select2 form-control" name="language" required >
						        	<option selected disabled> Select Language</option>
						        	<option value="en" <?php if($info[0]->language=='en'){echo "selected";}?> >English</option>
						        	<option value="bn" <?php if($info[0]->language=='bn'){echo "selected";}?> >Bengali</option>
						        </select>
						    </div>
					    </div>
					</div>
				</div>
				<div class="card-footer">
					<input type="submit" name="submit" class="btn btn-primary float-right" value="<?php echo lang('save');?>">
				</div>
			</div>
		<?php echo form_close();?>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
