<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
				    <h3 class="card-title m-b-0"><?php echo lang('all_backup');?></h3>
				</div>
				
					<table class="table table-bordered table-hover" id="data-table">
						<thead class="thead-dark">
						    <tr class="animated slideInLeft slow">
						        <th><?php echo lang('SL');?></th>
						        <th><?php echo lang('Name');?></th>
						        <th><?php echo lang('Action');?></th>
						    </tr>
						</thead>
						<tbody>
							<?php 
								if($fileLists != NULL){
							foreach ($fileLists as $key => $value) { ?>
							<tr>
								<td><?php echo $key+1;?></td>
								<td><?php echo $value;?></td>
								<td>

									<?php if($user_privilege != 'superadmin' && permissionCheck('Backup', $userid, 'view')){?>
									<a class="btn btn-success" title="Download" href="<?php echo base_url('./backup/').$value;?>"> Download</a>
									<?php }else{ ?>
									<a class="btn btn-success" title="Download" href="<?php echo base_url('./backup/').$value;?>"> Download</a>
									<?php } ?>

									<?php if($user_privilege != 'superadmin' &&  permissionCheck('Backup', $userid, 'delete')){?>
									<a onclick="return confirm('Do you want to delete this File?');" class="btn btn-danger" title="Delete" href="<?php echo site_url('backup/delete/').$value;?>"> Delete</a>
									<?php }else{ ?>
									<a onclick="return confirm('Do you want to delete this File?');" class="btn btn-danger" title="Delete" href="<?php echo site_url('backup/delete/').$value;?>"> Delete</a>
									<?php } ?>
								</td>
							</tr>
							<?php } } ?>
						</tbody>
						<tfoot class="thead-light">
						<tr class="animated slideInRight slow">
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							
						</tr>
						</tfoot>
					</table>
				<div class="card-footer">
					<div class="col-md-12"><h3>&nbsp;</h3></div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
