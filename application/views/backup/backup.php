<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->
    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-md-12">
            <div class="card">
            	<div class="card-header">
                    <h3 class="card-title"><?php echo lang('create_backup');?></h3>
                </div>
                <div class="card-body">
                	<div class="row">
                		<div class="col-md-6">
	                		<a href="<?php echo site_url('backup/createBackup/').'data';?>" class="btn btn-primary btn-lg float-right" onclick="return confirm('Are you sure to do this?');"><?php echo lang('database_backup');?></a>
						</div>
                        <div class="col-md-6">
	                		<a href="<?php echo site_url('backup/createBackup/').'project';?>" class="btn btn-success btn-lg" onclick="return confirm('Are you sure to do this?');"><?php echo lang('Software_backup');?></a>
                			
                		</div>
						
                	</div>
                </div>
                <div class="card-footer">
                    <h3>&nbsp;</h3>
                </div>
            </div>
        </div>  
    </div>
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->