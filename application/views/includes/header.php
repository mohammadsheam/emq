<!DOCTYPE html>
<html lang="en" ng-app="ngApp">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<!-- Tell the browser to be responsive to screen width -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="author" content="">
	<!-- Favicon icon -->
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url('private/assets/images/favicon.png');?>">
	<title><?php echo $title;?></title>
    <script src="<?php echo base_url('private/assets/libs/jquery/dist/jquery.min.js');?>"></script>
	<!-- Animate CSS -->
	<link href="<?php echo base_url('private/assets/libs/animateCSS/aniamte.css');?>" rel="stylesheet">
	<!-- Custom CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('private/assets/libs/select2/dist/css/select2.min.css');?>">
    <script src="<?php echo base_url('private/assets/libs/select2/dist/js/select2.min.js');?>"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('private/assets/libs/jquery-minicolors/jquery.minicolors.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('private/assets/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('private/dist/css/style.min.css');?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('private/assets/extra-libs/multicheck/multicheck.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('private/assets/libs/datatables.net-bs4/css/dataTables.bootstrap4.css');?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('private/assets/libs/toastr/build/toastr.min.css');?>" >

	<!-- angular js -->
	<script src="<?php echo base_url('private/dist/js/angular.js');?>"></script>
	<script src="<?php echo base_url('private/dist/js/angular-animate.js');?>"></script>
	<script src="<?php echo base_url('private/dist/js/angular-select2.js');?>"></script>
    
    <!-- load angular script -->
	<script type="text/javascript" src="<?php echo base_url('private/script/ngScript/app.js');?>"></script>

	<!-- load text editor -->
    <script src="<?php echo base_url('private/assets/libs/ckEditor/ckEditor.js');?>"></script>


    <script src="<?php echo base_url('private/assets/extra-libs/DataTables/datatables.min.js');?>"></script>
    <script src="<?php echo base_url('private/assets/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');?>"></script>

	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/DataTables/dataTables.buttons.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/DataTables/dataTables.responsive.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/DataTables/buttons.flash.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/DataTables/jszip.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/DataTables/pdfmake.min.js');?>"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/DataTables/buttons.html5.min.js');?>"></script>
	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/DataTables/buttons.print.min.js');?>"></script>

	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.dataTables.min.css">
	
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
	<script type="text/javascript" src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('private/assets/extra-libs/angular-datatables/dist/css/angular-datatables.min.css');?>">
	<script type="text/javascript" src="<?php echo base_url('private/assets/extra-libs/angular-datatables/dist/angular-datatables.min.js');?>"></script>
</head>

<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>