<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">

                <?php if($user_privilege != 'superadmin') { ?> <!-- for admin/user previliege -->
                    <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin');?>" aria-expanded="false">
                            <i class="mdi mdi-view-dashboard">&nbsp;</i>
                            <span class="hide-menu">Dashboard</span>
                        </a>
                    </li>
                    

                    <!-- Setting menu -->
                    <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fas fa-cog">&nbsp;</i>
                            <span class="hide-menu">Setting </span>
                         </a>
                        <ul aria-expanded="false" class="collapse animated slideInLeft first-level">
                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Profile'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Profile</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Profile/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Show Profile</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('User_permission/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">User Role</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Activity/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">User Activity</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Theme_setting'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Software Setting</span>
                                </a>
                            </li>
                        </ul>
                    </li>

                    <br><br>


                <?php }else{ ?> <!-- for superadmin -->

                    <li class="sidebar-item"> 
                        <a class="sidebar-link waves-effect waves-dark sidebar-link" href="<?php echo site_url('admin');?>" aria-expanded="false">
                            <i class="mdi mdi-view-dashboard">&nbsp;</i>
                            <span class="hide-menu">Dashboard</span>
                        </a>
                    </li>
                    

                    <!-- Setting menu -->
                    <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fas fa-cog">&nbsp;</i>
                            <span class="hide-menu">Setting </span>
                         </a>
                        <ul aria-expanded="false" class="collapse animated slideInLeft first-level">
                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Profile'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Profile</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Profile/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Show Profile</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('User_permission/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">User Role</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Activity/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">User Activity</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Theme_setting'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Software Setting</span>
                                </a>
                            </li>
                        </ul>
                    </li>


                    <li class="sidebar-item"><a class="sidebar-link has-arrow waves-effect waves-dark" href="javascript:void(0)" aria-expanded="false">
                            <i class="fas fa-cog">&nbsp;</i>
                            <span class="hide-menu">Website Setting </span>
                         </a>
                        <ul aria-expanded="false" class="collapse animated slideInLeft first-level">
                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Website_header/header'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Header</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Profile/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Header menu</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('User_permission/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Sidebar menu</span>
                                </a>
                            </li>

                            <li class="sidebar-item">
                                <a href="<?php echo site_url('Activity/all'); ?>" class="sidebar-link">
                                    <i class="fas fa-arrow-right"></i>
                                    <span class="hide-menu">Footer</span>
                                </a>
                            </li>

                        </ul>
                    </li>

                    <br><br>
                    
                <?php } ?>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">