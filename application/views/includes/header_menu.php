<!-- ============================================================== -->
<!-- Main wrapper - style you can find in pages.scss -->
<!-- ============================================================== -->
<div id="main-wrapper">
    <!-- ============================================================== -->
    <!-- Topbar header - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <header class="topbar" data-navbarbg="skin5">
        <nav class="navbar top-navbar navbar-expand-md navbar-dark">
            <div class="navbar-header" data-logobg="skin5">
                <!-- This is for the sidebar toggle which is visible on mobile only -->
                <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>
                <!-- ============================================================== -->
                <!-- Logo -->
                <!-- ============================================================== -->
                <a class="navbar-brand" href="<?php echo site_url('admin');?>">
                    <!-- Logo icon -->
                    <b class="logo-icon p-l-10">
                        <!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->
                        <!-- Dark Logo icon -->
                        <img src="<?php echo base_url('private/assets/images/logo-icon.png');?>" alt="homepage" class="light-logo" />                        
                    </b>
                    <!--End Logo icon -->
                    <!-- Logo text -->
                    <span class="logo-text">
                       <!-- dark Logo text -->
                       <img src="<?php echo base_url('private/assets/images/logo-text.png');?>" alt="homepage" class="light-logo" />
                   </span>
                </a>
                <!-- ============================================================== -->
                <!-- End Logo -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Toggle which is visible on mobile only -->
                <!-- ============================================================== -->
                <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
            </div>
            <!-- ============================================================== -->
            <!-- End Logo -->
            <!-- ============================================================== -->
            <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                <!-- ============================================================== -->
                <!-- toggle and nav items -->
                <!-- ============================================================== -->
                <ul class="navbar-nav float-left mr-auto">
                    <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                </ul>

            <a href="<?php echo site_url('home') ?>" class="btn btn-primary" target="__blank">Visit site</a>
            <!-- ============================================================== -->
            <!-- Right side toggle and nav items -->
            <!-- ============================================================== -->
            <ul class="navbar-nav float-right">
                
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <img src="<?php echo base_url('').$this->session->userdata('image');?>" alt="user" class="rounded-circle" width="31">
                </a>
                <div class="dropdown-menu dropdown-menu-right user-dd animated slideInDown">
                    <a class="dropdown-item"> <h5> <?php echo $this->session->userdata('fullname'); ?> </h5>[<?php echo $this->session->userdata('privilege'); ?>]</a>
                    <div class="dropdown-divider"></div>
                    <div style="margin-left: 12px;important!" class="p-l-30 p-10"><a href="<?php echo site_url('access/login/logout');?>" class="btn btn-sm btn-danger btn-rounded"><?php echo lang('Log_Out');?></a></div>
                    <div class="dropdown-divider"></div>
                    <div style="margin-left: 12px;important!" class="p-l-30 p-10"><a href="<?php echo site_url('profile/view');?>" class="btn btn-sm btn-success btn-rounded"><?php echo lang("Profile");?></a></div>
                </div>
            </li>
            <!-- ============================================================== -->
            <!-- User profile and search -->
            <!-- ============================================================== -->
        </ul>
    </div>
</nav>
</header>
<!-- ============================================================== -->
<!-- End Topbar header -->
        <!-- ============================================================== -->