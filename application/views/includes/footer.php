<!-- ============================================================== -->
<!-- footer -->
<!-- ============================================================== -->
<footer class="footer text-center">
    All Rights Reserved by <a href="https://www.sunnahsoft.com/" data-toggle="tooltip" title="Sunnah Soft!" target="__blank"><strong class="text-primary">Sunnah Soft</strong></a>.
</footer>
<!-- ============================================================== -->
<!-- End footer -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Page wrapper  -->
</div>
<!-- ============================================================== -->
<!-- End Wrapper -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->

<!-- Bootstrap tether Core JavaScript -->
<script src="<?php echo base_url('private/assets/libs/popper.js/dist/umd/popper.min.js');?>"></script>
<script src="<?php echo base_url('private/assets/libs/bootstrap/dist/js/bootstrap.min.js');?>"></script>
<script src="<?php echo base_url('private/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js');?>"></script>
<script src="<?php echo base_url('private/assets/extra-libs/sparkline/sparkline.js');?>"></script>
<!--Wave Effects -->
<script src="<?php echo base_url('private/dist/js/waves.js');?>"></script>
<!--Menu sidebar -->
<script src="<?php echo base_url('private/dist/js/sidebarmenu.js');?>"></script>
<!--Custom JavaScript -->
<script src="<?php echo base_url('private/dist/js/custom.min.js');?>"></script>

<script src="<?php echo base_url('private/assets/libs/jquery-minicolors/jquery.minicolors.min.js');?>"></script>

<script src="<?php echo base_url('private/assets/extra-libs/multicheck/datatable-checkbox-init.js');?>"></script>
<script src="<?php echo base_url('private/assets/extra-libs/multicheck/jquery.multicheck.js');?>"></script>

<script src="<?php echo base_url('private/assets/libs/toastr/build/toastr.min.js');?>"></script>


<script>
        //***********************************//
        // For select 2
        //***********************************//
        $(".select2").select2();

        $('.demo').minicolors({theme: 'bootstrap',position: 'bottom right',});

        /*datepicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
        	autoclose: true,
        	todayHighlight: true,
        	format: 'yyyy-mm-dd'
        });
        jQuery('.mydatepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });

        /* Data tables*/
        $('table.data-table').DataTable({
                dom:"<'row'<'col-sm-4'l><'col-sm-4 text-center'B><'col-sm-4'f>>tp", 
                //dom: 'C<"clear">lfrtip',

                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.modal( {
                            header: function ( row ) {
                                var data = row.data();
                                return 'Details for '+data[1];
                            }
                        } ),
                        renderer: $.fn.dataTable.Responsive.renderer.tableAll( {
                            tableClass: 'table'
                        } )
                    }
                },
                
                buttons:[
                {
                    extend: "csv", className: "btn btn-md btn-primary",footer: true,
                     exportOptions: {columns: ':not(:last-child)',}
                }
                , {
                    extend: "excel",  className: "btn btn-md btn-primary", footer: true,
                     exportOptions: {columns: ':not(:last-child)',}
                }
                , {
                    extend: "pdf",  className: "btn btn-md btn-primary",title: "",
                    //orientation: 'landscape',
                    pageSize: 'A4',
                    header: true,
                    footer: true,
                    customize: function ( doc ) {
                        doc.defaultStyle.fontSize = 12;
                        doc.styles.tableHeader.fontSize = 13;
                        doc.pageMargins = [10,30,10,30];
                        doc.defaultStyle.alignment = 'center';

                        //border styling
                        var objLayout = {};
                        objLayout['hLineWidth'] = function(i) { return .5; };
                        objLayout['vLineWidth'] = function(i) { return .5; };
                        objLayout['hLineColor'] = function(i) { return '#2d4154'; };
                        objLayout['vLineColor'] = function(i) { return '#2d4154'; };
                        objLayout['paddingLeft'] = function(i) { return 4; };
                        objLayout['paddingRight'] = function(i) { return 4; };
                        doc.content[0].layout = objLayout;
                    },
                    exportOptions: {columns: ':not(:last-child)',}
                    
                }
                , {
                    extend: "print", className: "btn btn-md btn-primary", title: "", footer: true,
                    exportOptions: {columns: ':not(:last-child)',}
                },
                ]

                

            });

         // select 2 dropdown 
            $("select.form-control:not(.dont-select-me)").select2({
                //placeholder: "Select option",
                allowClear: true
            });

        $(function(){
        	<?php echo $this->session->flashdata('notification'); ?>
        });

    </script>
</body>

</html>