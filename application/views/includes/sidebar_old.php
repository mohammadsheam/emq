<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->

<?php //print_r($controllers);?>

<aside class="left-sidebar" data-sidebarbg="skin5">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav" class="p-t-30">
                <li class="sidebar-item"> <a class="sidebar-link waves-effect waves-dark animated slideInLeft slow" href="<?php echo site_url('home');?>" aria-expanded="false"><i class="mdi mdi-view-dashboard"></i><span class="hide-menu"><?php echo lang('Dashboard');?></span></a></li>
                                
                <?php foreach ($controllers as $key => $controller) { 
                    if($user_privilege != 'superadmin'){
                        //if ($key !='Dashboard' && $key !='Login' && $key !='Myerror' && $key !='Api') {
                        if (!in_array($key, config_item('controllers'))) {
                            if( moduleCheck($key, $userid)=='checked') {
                ?>
                <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark animated slideInLeft slow" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu"><?php echo lang($key);?> </span></a>
                    <ul aria-expanded="false" class="collapse animated slideInLeft fast first-level">
                        <?php foreach ($controller as $method) { if( $method =='index' && permissionCheck($key, $userid, 'insert')){ ?>
                        <li class="sidebar-item"><a href="<?php echo site_url('').$key.'/'.$method;?>" class="sidebar-link"><i class="fas fa-angle-right"></i><span class="hide-menu"> <?php echo lang('add');?></span></a></li>
                        <?php } elseif($method =='all' && permissionCheck($key, $userid, 'view')){ ?>
                        <li class="sidebar-item"><a href="<?php echo site_url('').$key.'/'.$method;?>" class="sidebar-link"><i class="fas fa-angle-right"></i><span class="hide-menu"> <?php echo lang('all')?></span></a></li>
                        <?php } }?>
                    </ul>
                </li>

                <?php } } }else{
                        if (!in_array($key, config_item('controllers'))) {
                ?>
                    <li class="sidebar-item"> <a class="sidebar-link has-arrow waves-effect waves-dark animated slideInLeft slow" href="javascript:void(0)" aria-expanded="false"><i class="mdi mdi-border-inside"></i><span class="hide-menu"><?php echo lang($key);?> </span></a>
                        <ul aria-expanded="false" class="collapse animated slideInLeft fast first-level">
                            <?php foreach ($controller as $method) { if( $method =='index'){ ?>
                            <li class="sidebar-item"><a href="<?php echo site_url('').$key.'/'.$method;?>" class="sidebar-link"><i class="fas fa-angle-right"></i><span class="hide-menu"> <?php echo lang('add');?></span></a></li>
                            <?php } if($method =='all'){ ?>
                            <li class="sidebar-item"><a href="<?php echo site_url('').$key.'/'.$method;?>" class="sidebar-link"><i class="fas fa-angle-right"></i><span class="hide-menu"> <?php echo lang('all')?></span></a></li>
                            <?php } }?>
                        </ul>
                    </li>
                <?php } } } ?>
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Page wrapper  -->
<!-- ============================================================== -->
<div class="page-wrapper">