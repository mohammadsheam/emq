<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
				    <h3 class="card-title m-b-0"><?php echo lang('all');?> Users</h3>
				</div>
				
					<table class="table table-bordered table-hover" id="data-table">
						<thead class="thead-dark">
						    <tr >
						        <th><?php echo lang('SL');?></th>
						        <th><?php echo lang('Username');?></th>
						        <th><?php echo lang('permission');?></th>
						    </tr>
						</thead>
						<tbody>
							<?php 
								if($users != NULL){
							foreach ($users as $key => $user) {
								if($this->session->userdata('user_id') != $user->id){
							 ?>
							<tr class="animated slideInUp slow">
								<td><?php echo $key+1;?></td>
								<td><?php echo $user->fullname;?></td>
								<td>
									<a title="Set Permission" href="<?php echo base_url('User_permission/setPermission/'.$user->id);?>" class="btn btn-info">
										<i class="mdi mdi-brightness-7" aria-hidden="true"></i>
									</a>
								</td>
							</tr>
							<?php } } }?>
						</tbody>
						<tfoot class="">
						<tr class="animated slideInRight slow">
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							<th>&nbsp;</th>
							
						</tr>
						</tfoot>
					</table>
				<div class="card-footer">
					<div class="col-md-12">&nbsp;</div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
