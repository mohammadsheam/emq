<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
				    <p class="card-title m-b-0">
						<h3 class="float-left"></i><?php echo lang('permission');?> for <?php echo $users[0]->fullname ?> </h3>
						<a href="<?php echo site_url('User_permission/all');?>" class="btn btn-primary float-right"><i class="mdi mdi-keyboard-return"></i><?php echo lang('Back');?></a>
					</p>
				</div>
				<?php echo form_open('', '');?>
					<table class="table table-bordered table-hover" id="data-table">
						<thead class="thead-dark">
						    <tr class="animated slideInLeft slow">
						    	<th width="40px">
						    		<label class="customcheckbox m-b-20">
                                        <input type="checkbox" id="mainCheckbox" />
                                        <span class="checkmark"></span>
                                    </label>
						    	</th>
						        <th><?php echo lang('SL');?></th>
						        <th><?php echo lang('Menu_name');?></th>
						        <th><?php echo lang('add');?></th>
						        <th><?php echo lang('View');?></th>
						        <th><?php echo lang('Edit');?></th>
						        <th><?php echo lang('Delete');?></th>
						    </tr>
						</thead>
						<tbody>
							<?php 
								if($controllers != NULL){ $count = 1;
							foreach ($controllers as $key => $controller) {

								if($user_privilege != 'superadmin'){
                                    if (!in_array($key, config_item('controllers'))) {
							 ?>
							<tr>
								<td><label class="customcheckbox">
		                                <input type="checkbox" class="listCheckbox" name="con[]" <?php echo moduleCheck($key, $users[0]->id);?> value="<?php echo $key;?>" />
		                                <span class="checkmark"></span>
		                            </label>
		                        </td>
								<td><?php echo $count++;?></td>
								<td><?php echo lang($key);?></td>
								<td><label class="customcheckbox">
		                                <input type="checkbox" name="insert[]" <?php echo permissionCheck($key, $users[0]->id, 'insert');?> />
		                                <span class="checkmark"></span>
		                            </label>
		                        </td>
		                        <td><label class="customcheckbox">
		                                <input type="checkbox" name="view[]" <?php echo permissionCheck($key, $users[0]->id, 'view');?> />
		                                <span class="checkmark"></span>
		                            </label>
		                        </td>
		                        <td><label class="customcheckbox">
		                                <input type="checkbox" name="edit[]" <?php echo permissionCheck($key, $users[0]->id, 'edit');?> />
		                                <span class="checkmark"></span>
		                            </label>
		                        </td>
		                        <td><label class="customcheckbox">
		                                <input type="checkbox" name="delete[]" <?php echo permissionCheck($key, $users[0]->id, 'delete');?> />
		                                <span class="checkmark"></span>
		                            </label>
		                        </td>
		                        
							</tr>
							<?php }}else{

                                    if (!in_array($key, config_item('controllers'))) { ?>
									<tr>
										<td><label class="customcheckbox">
				                                <input type="checkbox" class="listCheckbox" name="con[]" <?php echo moduleCheck($key, $users[0]->id);?> value="<?php echo $key;?>" />
				                                <span class="checkmark"></span>
				                            </label>
				                        </td>
										<td><?php echo $count++;?></td>
										<td><?php echo lang($key);?></td>
										<td><label class="customcheckbox">
				                                <input type="checkbox" name="insert[]" <?php echo permissionCheck($key, $users[0]->id, 'insert');?> />
				                                <span class="checkmark"></span>
				                            </label>
				                        </td>
				                        <td><label class="customcheckbox">
				                                <input type="checkbox" name="view[]" <?php echo permissionCheck($key, $users[0]->id, 'view');?> />
				                                <span class="checkmark"></span>
				                            </label>
				                        </td>
				                        <td><label class="customcheckbox">
				                                <input type="checkbox" name="edit[]" <?php echo permissionCheck($key, $users[0]->id, 'edit');?> />
				                                <span class="checkmark"></span>
				                            </label>
				                        </td>
				                        <td><label class="customcheckbox">
				                                <input type="checkbox" name="delete[]" <?php echo permissionCheck($key, $users[0]->id, 'delete');?> />
				                                <span class="checkmark"></span>
				                            </label>
				                        </td>
				                        
									</tr>
							<?php } } } }?>
						</tbody>
						<tfoot class="">
						<tr class="animated slideInUp slow">
							<th colspan="7">
						<input type="submit" name="submit" class="btn btn-primary float-right" value="<?php echo lang('save');?>">
							</th>							
						</tr>
						</tfoot>
					</table>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
