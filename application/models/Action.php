<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Action extends My_Model {

	function __construct(){
		parent::__construct();
	}
	

	// save into database
    public function add($table, $data) {
        $this->db->insert($table, $data);
        return true;
    }

    // read from database
    public function read($table, $where=array()){

        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();

    }

    // update table data
    public function update($table, $data, $where= array()){
        $this->db->set($data);
        $this->db->where($where);
        $this->db->update($table);
        return true;
    }


    // delete data 
    public function delete($table, $where=array()){
        $this->db->where($where);
        $this->db->delete($table);
        return true;
    }


    // check exist
    public function existance($table, $where=array()){

    }

    //save data into db and return auto increment ID
    public function addAndGetID($table, $data){
        $this->db->insert($table,$data);
        $insert_id = $this->db->insert_id();
        return  $insert_id;
    }


    // read from database using two table
    public function joinAndRead($from, $join, $joinCond, $where=array()){
        $this->db->select('*');
        $this->db->from($from);
        $this->db->join($join, $joinCond);
        $this->db->where($where);
        $query = $this->db->get();
		return $query->result();
    }


    // read limit
    public function read_limit($table, $where=array(), $order='id', $order_by='DESC', $limit=1){
        $this->db->where($where);
        $this->db->order_by($order, $order_by);
        $this->db->limit($limit);
        $query = $this->db->get($table);
        return $query->result();
    }

    // read sum of a column
    public function read_sum($table, $col, $where=array()){
        $this->db->select_sum($col);
        $this->db->from($table);
        $this->db->where($where);

        $query = $this->db->get();
        return $query->result();
    }

    public function readGroupBy($table, $groupBy, $where=array(), $orderBy="id", $sort="desc"){
        $this->db->select('*');
        $this->db->group_by($groupBy);
        $this->db->order_by($orderBy, $sort);
        $this->db->where($where);
        $query = $this->db->get($table);

        return $query->result();
    }

    
    /**
     * @param  [string] multiple coulmn name will be separated by comma
     * @param  [string] table name.
     * @param  [array] condtion
     * @return [dataset] object
     */
    public function read_column($column, $table, $where){
        
        $this->db->select($column);
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result();
    }
}

/* End of file action.php */
/* Location: ./application/models/action.php */