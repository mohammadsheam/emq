<?php

/**
 * Custom methods/functions are define here
 */

//Function for Dynamic Language Start
function lang($index){
    $CI = & get_instance();
    // load model
    $CI->load->model('action');
    // use model method
    $val = $CI->action->read('theme_setting');

    $label_lang = config_item('label_lang');


    return $label_lang[$index][$val[0]->language];

}
//Function for Dynamic Language End



/**
 * add language array to config array
 * @param  [string] $label [array index name]
 * @param  [string] $en    [english name]
 * @param  [string] $bn    [bangla name]
 * @return [boolean]        [true/false]
 */
function config_lang($label, $en, $bn){

    $fileName = './application/config/my_language.php';

    $item = "
	'".$label."' =>array(
		'bn'=>'".$bn."',
		'en'=>'".ucfirst(str_replace("_", " ", $en))."'
	), 
);";

    $contents = file($fileName);

    $new_contents = array();
    foreach ($contents as $key => $value) {
        $new_contents[] = $value;
    }

    $new_contents[$key] = $item;

    file_put_contents($fileName, implode('',$new_contents));

    return true;
}


/**
 * dynamically generate controller
 * @param  [string] $class [class name]
 * @param  [string] $tableName [table name]
 * @return [boolean]        [true/false]
 */
function generateClass($class, $tableName, $colname){

    $header = "<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ".ucfirst($class)." extends Backend {

	function __construct(){
		parent::__construct();
	}


	/**
	 * [Defult function.. handle insertion of data after submit form data]
	 * @return [none] [nothing return]
	 */
	public function index(){
		\$this->data['title'] =  lang('".ucfirst($class)."').' | '.lang('add');
		\$this->data['notification'] = null;

		if(\$this->input->post('submit')){
			\$data = array(
				";

    $dataArray = '';
    foreach ($colname as $key => $name) {
        $index = "'".str_replace(' ','_',trim($name, ' '))."' => \$this->input->post('".str_replace(' ','_',trim($name, ' '))."'),
					";
        $dataArray = $dataArray.$index;
    }

    $rest = ");
			
			// insert data to database table
			\$status = \$this->action->add('".$tableName."', \$data);

			if(\$status){
				\$msg =  message('success', 'Data Inserted Successfully!', 'Done');
			}else{
				 \$msg = message('error', 'Everything created successfully!', 'Opps!');
			}

			\$this->session->set_flashdata('notification', \$msg);
			redirect('".ucfirst($class)."/index','refresh');
		}
		
		\$this->load_page('".$class."/add');
	}


	/**
	 * fetch all data from database table
	 * @return [array] [dataset array]
	 */
	public function all(){
		\$this->data['title'] = lang('".ucfirst($class)."').' | '.lang('all');
		\$this->data['notification'] = null;

		
		\$this->data['result'] = \$this->action->read('".$tableName."');

		\$this->load_page('".$class."/all');
	}
	


	/**
	 * fetch data from database table by userid
	 * update data using userid
	 * @return [response] [sucess msg]
	 */
	public function edit(\$id){
		\$this->data['title'] = lang('".ucfirst($class)."').' | '.lang('Edit');
		\$this->data['notification'] = null;
		
		\$where = array('id' => \$id);
		\$this->data['record'] = \$this->action->read('".$tableName."', \$where);
		
		if(\$this->input->post('update')){
			\$data = array(
					";
    $editArray = '';
    foreach ($colname as $key => $name) {
        $index = "'".str_replace(' ','_',trim($name, ' '))."' => \$this->input->post('".str_replace(' ','_',trim($name, ' '))."'),
					";
        $editArray = $editArray.$index;
    }

    $editRest = ");

				// update data to database table
				\$where = array('id' => \$this->input->post('id'));
				\$status = \$this->action->update('".$tableName."', \$data, \$where);

				if(\$status){
					\$msg =  message('success', 'Data Updated Successfully!', 'Done');
				}else{
					 \$msg = message('error', 'Everything created successfully!', 'Opps!');
				}

				\$this->session->set_flashdata('notification', \$msg);
				redirect('".ucfirst($class)."/all','refresh');

		}


		\$this->load_page('".$class."/edit');
	}


	/**
	 * Delete data from database table
	 * @param  [int] \$id [database table auto increment ID]
	 * @return [boolean]     [true/false]
	 */
	public function delete(\$id){
		\$where = array('id' => \$id);
		\$status = \$this->action->delete('".$tableName."', \$where);

		if(\$status){
			\$msg =  message('success', 'Data Delete Successfully!', 'Done');
		}else{
			\$msg =  message('error', 'Error in Data deletation', 'Opps!');
		}
		\$this->session->set_flashdata('notification', \$msg);
		redirect('".ucfirst($class)."/all','refresh');
	}

}";

    $data = $header.$dataArray.$rest.$editArray.$editRest;

    // define path and file name
    $fileName = './application/controllers/'.ucfirst($class).'.php';

    if ( ! write_file($fileName, $data)){
        return false;
    }
    else{
        return true;
    }


}


/**
 * Dynamically generate view folder and template file
 * @param  [string] $folder [class name will be used as view folder name]
 * @param  [array] $colname [all field name]
 * @param  [array] $field_type [all input field type]
 * @return [boolean]         [true/false]
 */
function generateView($folder, $colname, $field_type){
    clearstatcache();

    $path = './application/views/'.$folder;
    if ( !file_exists( $path ) ){
        mkdir($path, 0777, true);


        /**
         * ************ create add.php page here******************
         */

        $header = "
<style type='text/css'>
	.req {
		color: red;
		font-weight: bold;
	}

	.op {
		color: gray;
		font-weight: lighter;
	}

</style>

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class='container-fluid'>
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class='row'>
		<div class='col-md-12'>
		<?php echo form_open_multipart('', ''); ?>
			<div class='card'>
				<div class='card-header'>
				    <h3 class='card-title m-b-0'><?php echo lang('add');?></h3>
                </div>
				<div class='card-body'>
		" ;

        $body = " ";
        foreach ($colname as $key => $name) {
            $label = "
				<div class='form-group row'>
				    <label class='col-sm-2 text-right'><?php echo lang('".str_replace(" ", "_",trim($name," "))."');?><span class='req'> *</span></label>
				    <div class='col-sm-6'>
				        <input type='".str_replace(" ", "_",trim($field_type[$key]," "))."' name='".str_replace(" ", "_",trim($name," "))."' class='form-control' required>
				    </div>
				</div>

			";

            $body = $body.$label;

        }

        $footer = "
			</div>
			<div class='card-footer'>
				<input type='submit' name='submit' class='btn btn-primary float-right' value='<?php echo lang('save');?>'>
			</div>
		</div>
	<?php echo form_close();?>
	</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

";


        // write add file in the destination path/folder
        $fileName = $path.'/add.php';
        $data = $header.$body.$footer;
        write_file($fileName, $data);


        /**
         * ******************* add.php creation end here*****************
         */


        /**
         * ******** create all.php page ***************
         */

        $header = "
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class='container-fluid'>
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class='row'>
		<div class='col-12'>
			<div class='card'>
				<div class='card-body'>
				    <h3 class='card-title m-b-0'><?php echo lang('all');?></h3>
				</div>
				<table class='table table-bordered table-hover' id='data-table'>
					<thead class='thead-dark'>
					    <tr class='animated slideInLeft slow'>
					        <th><?php echo lang('SL');?></th>";

        $th = "";
        foreach ($colname as $name) {
            $throw = "
					        <th><?php echo lang('".str_replace(" ", "_",trim($name," "))."');?></th>";
            $th = $th.$throw;
        }

        $tbody = "
					 		<th><?php echo lang('Action') ;?></th>
					    </tr>
					</thead>
						<tbody>
							<?php 
								if(\$result != NULL){
							foreach (\$result as \$key => \$value) { ?>
							<tr>
								<td><?php echo \$key+1;?></td>
							";
        $td = "";
        foreach ($colname as $key => $name) {

            $tdrow = "<td><?php echo \$value->".str_replace(" ","_",trim($name, " ")).";?></td>
								";
            $td = $td.$tdrow;
        }
        $tfoot = "
								<td>
									<?php if(\$user_privilege != 'superadmin' && permissionCheck('".ucfirst($folder)."', \$userid, 'edit')){?>
									<a title='Edit' class='btn btn-warning' href='<?php echo site_url('".ucfirst($folder)."/edit/').\$value->id;?>'><i class=\"mdi mdi-table-edit\" aria-hidden=\"true\"></i></a>
									<?php }else{ ?>
									<a title='Edit' class='btn btn-warning' href='<?php echo site_url('".ucfirst($folder)."/edit/').\$value->id;?>'><i class=\"mdi mdi-table-edit\" aria-hidden=\"true\"></i></a>	
									<?php } ?>

									<?php if(\$user_privilege != 'superadmin' &&  permissionCheck('".ucfirst($folder)."', \$userid, 'delete')){?>
									<a onclick=\"return confirm('Do you want to delete this File?');\" class='btn btn-danger' title='Delete' href='<?php echo site_url('".ucfirst($folder)."/delete/').\$value->id;?>'> <i class=\"mdi mdi-delete\" aria-hidden=\"true\"></i></a>
									<?php }else{ ?>
									<a onclick=\"return confirm('Do you want to delete this File?');\" class='btn btn-danger' title='Delete' href='<?php echo site_url('".ucfirst($folder)."/delete/').\$value->id;?>'> <i class=\"mdi mdi-delete\" aria-hidden=\"true\"></i></a>
									<?php } ?>
								</td>
							</tr>
							<?php } }else{ ?>
								<tr class='animated slideInRight slow'>
									<td colspan='".($key+2)."'> <h4 class='text-danger text-center'>No data Found </h4>
									</td>
								</tr>
							<?php } ?>
						</tbody>
						<tfoot class='thead-light'>
							<tr class='animated slideInDown slow'>
								<th>&nbsp;</th>";

        $tfootth = "";
        foreach ($colname as $name) {
            $tfootthrow = "
								<th>&nbsp;</th>
							";
            $tfootth = $tfootth.$tfootthrow;
        }
        $rest ="<th>&nbsp;</th>
							</tr>
						</tfoot>
					</table>
		";

        $footer = "
				<div class=\"card-footer\">
					<div class=\"col-md-12\"><h3>&nbsp;</h3></div>
				</div>
			</div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- End PAge Content -->
	<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->
";


        // write all file to the destination folder
        $fileName = $path.'/all.php';
        $data = $header.$th.$tbody.$td.$tfoot.$tfootth.$rest.$footer;
        write_file($fileName, $data);


        /**
         * ************ all.php creation end here***************
         */




        /**
         * ****************** create edit.php here ***************
         */


        $header = "
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class='container-fluid'>
	<!-- ============================================================== -->
	<!-- Start Page Content -->
	<!-- ============================================================== -->
	<div class='row'>
		<div class='col-md-12'>
		<?php echo form_open('', ''); ?>
			<div class='card'>
				<div class='card-header'>
					<p class='card-title m-b-0'>
					    <h3 class=\"float-left\"><?php echo lang('Update');?></h3>
					    <a href=\"<?php echo site_url('".ucfirst($folder)."/all');?>\" class=\"btn btn-primary float-right\"><i class=\"mdi mdi-keyboard-return\"></i><?php echo lang('Back');?></a>
				    </p>
                </div>
				<div class='card-body'>
			        <input type='hidden' name='id' class='form-control' value='<?php echo \$record[0]->id ;?>' >
		" ;

        $body = " ";
        foreach ($colname as $key => $name) {
            $label = "
					<div class='form-group row'>
					    <label class='col-sm-2 text-right'><?php echo lang('".str_replace(" ", "_",trim($name," "))."');?></label>
					    <div class='col-sm-6'>
					        <input type='".str_replace(" ", "_",trim($field_type[$key]," "))."' name='".str_replace(" ", "_",trim($name," "))."' class='form-control' value='<?php echo \$record[0]->".str_replace(" ", "_",trim($name," "))." ;?>'>
					    </div>
					</div>
			";

            $body = $body.$label;

        }

        $footer = "
			</div>
			<div class='card-footer'>
				<input type='submit' name='update' class='btn btn-success float-right' value='<?php echo lang('Update');?>'>
			</div>
		</div>
	<?php echo form_close();?>
	</div>
</div>
<!-- ============================================================== -->
<!-- End PAge Content -->
<!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->
<!-- ============================================================== -->

";


        // write add file in the destination path/folder
        $fileName = $path.'/edit.php';
        $data = $header.$body.$footer;
        write_file($fileName, $data);


        /**
         * ******************** edit.php end here *********************
         */

        return true;
    }else{
        return false;
    }
}


// check field check or not
function moduleCheck($con, $id){
    // get class instance
    $CI = & get_instance();
    $CI->load->model('action');

    // read user data from database table
    $where = array('id' =>$id);
    $user_info = $CI->action->read('users', $where);

    $permissionList = json_decode($user_info[0]->permission);
    $status = '';
    if( count($permissionList) > 0){
        foreach ($permissionList as $key => $value) {
            if($con == $value->module_name){
                $status = 'checked';
            }
        }
    }

    return $status;
}


/**
 * check action permission of a module
 * @param  [string] $con  [module name]
 * @param  [int] $id   [user id]
 * @param  [string] $type [action type]
 * @return [string]       [variable]
 */
function permissionCheck($con, $id, $type){
    // get class instance
    $CI = & get_instance();
    $CI->load->model('action');

    // read user data from database table
    $where = array('id' =>$id);
    $user_info = $CI->action->read('users', $where);

    $permissionList = json_decode($user_info[0]->permission);

    $status = '';
    if( count($permissionList) > 0){
        foreach ($permissionList as $key => $value) {
            if($con == $value->module_name){
                if($value->$type == true){
                    $status = 'checked';
                }
            }
        }
    }


    return $status;
}

/**
 * Upload image only for clients
 * @return string upload path
 */
function uploadImage(){
    // load libraries
    $CI = & get_instance();

    $CI->load->library('upload');

    $config['upload_path'] = './private/assets/images/clients/';
    $config['allowed_types'] = 'png|jpeg|jpg|gif';
    $config['max_size'] = '5106';
    $config['max_width'] = '5000'; /* max width of the image file */
    $config['max_height'] = '5000';
    $config['file_name'] ="clients_".date('Y-m-d').'_'.generate_id('clients');
    $config['overwrite']=true;

    $CI->upload->initialize($config);

    if ($CI->upload->do_upload("image_file")){
        $upload_data = $CI->upload->data();
        $path= "private/assets/images/clients/".$upload_data['file_name'];
    }

    return $path;
}


/**
 * Auto generate Id
 */
function generate_id($table){

    $ci = & get_instance();
    $ci->load->model('action');
    $data = $ci->action->read_limit($table);
    if(count($data) > 0){
        $counter = intval($data[0]->id) + 1;
    } else {
        $counter = 1;
    }
    $code = date('m').date('d').str_pad($counter, 3, '0', STR_PAD_LEFT);
    return $code;
}


function balanceClass($balance){
    $class = ($balance < 0)? 'text-danger': 'text-success';
    return $class;
}

function activityLog($id=0,$module, $action){
    $data = array(
        'date' => date('Y-m-d'),
        'time' => date('h:i:s A'),
        'user_id' => $id,
        'module' => $module,
        'action' => $action,
    );

    $ci = & get_instance();
    $ci->load->model('action');
    return $ci->action->add('activity', $data);
}

function f_number($data){
    return number_format($data, 2);
}

function getName($table, $where=array()){
    $ci = & get_instance();
    $ci->load->model('action');

    $info = $ci->action->read($table, $where);

    $name = ($info)? $info[0]->name:'N/A' ;
    return $name;
}


function auth_key($header_key){
	$api_key = config_item('api_key');
	if($api_key == $header_key){
		return true;
	}else{
		return false;
	}
}


function getTotalColumn($partyId, $col){
	$ci = & get_instance();
    $ci->load->model('action');

    $table = 'party_transaction';
    $where = array('trash' => 0, 'party_id' => $partyId);
    $data = $ci->action->read_sum($table, $col, $where);

    return ($data[0]->$col !='')? $data[0]->$col : 0.0;

}