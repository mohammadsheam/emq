<?php
// handling ajax calls

function currentBalance($id){
    $CI = & get_instance();
    $CI->load->model('action');

    $where = array('party_id' => $id, 'trash' => 0);
    $data = $CI->action->read('party_transaction', $where);

    $total = 0.0;
    if($data){
        foreach ($data as $value){
            $total += ($value->credit - $value->debit);
        }
    }
    return $total;
}


/**
 * @param $id client id
 * @return int total sale order
 */
function totallOrder($id){
    $CI = & get_instance();
    $CI->load->model('action');

    $where = array('client_id' => $id,'trash' => 0);
    $data = $CI->action->read('sale', $where);
    $total = count($data);

    return $total;
}


/**
 * @param $id client ID
 * @return int pending sale order
 */
function pendingOrder($id){
    $CI = & get_instance();
    $CI->load->model('action');

    $where = array('client_id' => $id,'trash' => 0, 'status' => 'pending');
    $data = $CI->action->read('sale', $where);
    $total = count($data);

    return $total;
}
