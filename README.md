# Admin Panel
> under development

**Idea is to build up a dynamic Admin Panel with latest feauters.**


## Table of Content
- [Description](#description)
- [Installation](#installation)
- [Usage](#usage)
- [Documentation](#documentation)


## Description

| Techology | Description |
| ----------| ------------|
| Language  | PHP 7       |
| Framework | Codeigniter3|
| Database  | MySQL       |
| Template  | Bootstrap 4 |
| JavaScript| AngularJs   |


## Installation
- Download XAMPP [ 7.0+ version from the internet ]
[CLICK HERE TO DOWNLOAD](https://www.apachefriends.org/download.html) - install it to your pc
- goto htdocts folder and copy command from the below
```
git clone https://github.com/mohamadsheam/pharmacy_store.git pharmacy_store
```
- create database & name it to `pharmacy_store`
- import tables from the sql file which located in pharmacy_store/back_up folder
- [HIT IN THE ADDRESS BAR](http://localhost/pharmacy_store) - http://localhost/pharmacy_store


## Usage

> username: admin
> password : admin


## Documentation

**will comming soon**


