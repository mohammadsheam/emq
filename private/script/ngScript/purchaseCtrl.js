app.controller("purchaseCtrl", ["$scope", "$http", function($scope, $http){

	$scope.productList = [];
	$scope.productIDList = [];
	$scope.allProducts = [];

	$scope.amount = {
		'paid': 0.0,
		'subtotal' : 0.0,
		'present_balance' : 0.0,
		'discount' : 0.0,
		'grandTotal': 0.0
	};

	$scope.exist = false;
	$scope.batchExist = false;

	$scope.addItemFn = function(form){

		if(typeof $scope.product.product_id !== 'undefined' && $scope.product.product_id !==''){
			if($scope.productIDList.indexOf($scope.product.product_id) === -1){

				var cond = {
					'table' : 'product',
					'cond' : {
						'product_id' : $scope.product.product_id
					}
				};

				$http({
					method : "POST",
					url    : url + "read",
					data   : cond
				}).then(function(response){
					//console.log(response.data);
					if(response.data.length > 0){

						var item = {
							'product_id' : response.data[0].product_id,
							'name'       : response.data[0].name,
							'batch_id'   : '',
							'expire_date': '',
							'purchase'   : parseFloat(response.data[0].purchase),
							'sale'       : parseFloat(response.data[0].sale),
							'unit'       : response.data[0].unit,
							'category'   : response.data[0].category,
							'brand'      : response.data[0].brand,
							'quantity'   : 1,
							'subtotal'   : 0.00
						};
					}

					$scope.productList.push(item);

				},function(errors){
					//console.log(errors);
				});

				//add product id for check duplicate entry
				$scope.productIDList.push($scope.product.product_id);

			}
		}
		$scope.product= '';
		$scope.batch_id = '';
		form.$setPristine();
	};

	$scope.existCheck = function(item){
		if($scope.productIDList.indexOf(item.product_id) === -1){
			$scope.exist = false;
		}else{
			$scope.exist = true;
		}
	}

	$scope.setSubtotalFn = function(index){
		$scope.productList[index].subtotal = $scope.productList[index].purchase * $scope.productList[index].quantity;
		return $scope.productList[index].subtotal.toFixed(2);
	};

	$scope.getTotalQtyFn = function () {
        var total = 0.0;
        angular.forEach($scope.productList, function(item){
            total += parseFloat(item.quantity);
        });
       return total;
    };

	$scope.getSubTotalFn = function(){
		var total = 0.0;
		angular.forEach($scope.productList, function(item){
			total += parseFloat(item.subtotal);
		});
		$scope.amount.subtotal = total;

		return total.toFixed(2);
	};


	$scope.getGrandTotalFn = function(){
		var total = $scope.amount.subtotal - $scope.amount.discount;
		$scope.amount.grandTotal = total;
		return total.toFixed(2);
	};


	$scope.supplierBalanceFn = function(){

		if(typeof $scope.supplier_id !=='undefined'){

			var where = {
				'table' : 'suppliers',
				'id'    : $scope.supplier_id,
				'cond'   : {'supplier_id' : $scope.supplier_id }
			};

			$http({
				method: "POST",
				url   : url + "partyInfo",
				data  : where
			}).then(function(response){
				console.log(response.data);
				$scope.amount.present_balance = response.data.balance;
			});
		}
	};

	$scope.getCurrentBalanceFn = function(){
		var total = $scope.amount.present_balance + $scope.amount.paid - $scope.amount.grandTotal;
		return total.toFixed(2);
	};

	$scope.deleteItemFn = function(index){
		$scope.productIDList.splice(index, 1);
		return $scope.productList.splice(index, 1);
	};

	$scope.getProductBySupplier = function(){
		var cond = {
			'table' : 'product',
			'cond' : {
				'supplier_id' : $scope.supplier_id
			}
		};

		$http({
			method: "POST",
			url   : url + "read",
			data  : cond
		}).then(function(response){
			//console.log(response);
			if(response.data.length > 0){
				$scope.allProducts = response.data;
			}else{
				$scope.allProducts = response.data;
			}
		});
	}

	// check batch id already exit or not
	$scope.checkBatchId = function(index){
		var cond = {
			'table' : 'stock',
			'cond' : {
				'product_id' : $scope.productList[index].product_id,
				'batch_id'   : $scope.productList[index].batch_id,
				'trash'		 : '0'
			}
		};
		$http({
			method: "POST",
			url   : url + "read",
			data : cond,
		}).then(function(response){
			if(response.data.length > 0){
				$scope.batchExist = true;
				alert("This Batch ID already Exist!!")
			}else{
				$scope.batchExist = false;
			}
		});
	}
}]);