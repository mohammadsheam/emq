app.controller("supplierTrxCtrl",["$scope", "$http", function ($scope, $http) {
    $scope.paid = 0;
    $scope.pre_balance = 0;

    // get client Info
    $scope.supplierInfo = function () {

        $scope.info = {};

        var condition = {
            'table' : 'suppliers',
            'id'    : $scope.supplier_id,
            'cond'  : {'supplier_id' : $scope.supplier_id }
        };

        //console.log(url);

        $http({
            method : "POST",
            url : url + "partyInfo",
            data : condition
        }).then(function (response) {
            //console.log(response.data);
            $scope.info = response.data;
            $scope.pre_balance = parseFloat($scope.info.balance);
        },function (errors) {
            //console.log(errors);
        });

    };


    // calculate balance here
    $scope.presentBalance = function () {
        var total = $scope.pre_balance + $scope.paid;
        return total.toFixed(2);
    }
}]);