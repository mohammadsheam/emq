var app = angular.module("ngApp", ['ngAnimate', 'willcrisis.angular-select2', 'datatables', ]);
// server side API
var url  = window.location.origin + '/pharmacy_store/Ajax/'; // this line will be commented in the server.
//var url  = window.location.origin + '/Ajax'; // this line will be un commented in the server.
//
app.directive("datepicker", function () {

    function link(scope, element, attrs) {
        // CALL THE "datepicker()" METHOD USING THE "element" OBJECT.
        element.datepicker({
            //dateFormat: "yyyy-mm-dd"
            format: 'yyyy-mm-dd'
        });
    }

    return {
        require: 'ngModel',
        link: link
    };
});