app.controller("clientTrxCtrl",["$scope", "$http", function ($scope, $http) {
    $scope.paid = 0;
    $scope.pre_balance = 0;

    // get client Info
    $scope.clientInfo = function () {

        $scope.info = {};

        var condition = {
            'table' : 'clients',
            'id'    : $scope.client_id,
            'cond'  : {'client_id' : $scope.client_id }
        };

        //console.log(url);

        $http({
            method : "POST",
            url : url + "partyInfo",
            data : condition
        }).then(function (response) {
            console.log(response.data);
            $scope.info = response.data;
            $scope.pre_balance = parseFloat($scope.info.balance);
        },function (errors) {
            //console.log(errors);
        });

    };

    // calculate balance here
    $scope.presentBalance = function () {
        var total = $scope.pre_balance - $scope.paid;
        return total.toFixed(2);
    }

}]);
