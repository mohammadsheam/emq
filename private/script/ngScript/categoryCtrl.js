app.controller("categoryCtrl",["$scope", "$http", function($scope, $http) {

	$scope.exist = false;
	$scope.result = [];

	$scope.existCheck = function(){
		$scope.exist = false;
		if($scope.unit_name != null || $scope.unit_name==''){

			var name = $scope.unit_name.toLowerCase();
			slug = name.trim().replace(" ","_");

			var where = {
				'table': 'category',
				'cond' : {
					'slug'  : slug,
					'trash' : '0'
				}
			}

			$http({
				method : "POST",
				url    : url + "read",
				data   : where
			}).then(function(response){
				//console.log(response);
				if (response.data.length > 0) {
					$scope.exist = true;
				}else{
					$scope.exist = false;
				}
			});
		}
	}


	// fetch defualt data
	var loadData = function(){
		var where = {
			'table' : 'category',
			'cond'  : {
				'trash' : '0'
			}
		};

		$http({
			method : "POST",
			url    : url + "read",
			data   : where
		}).then(function(response){
			if(response.data.length > 0){
				$scope.result = response.data;
			}else{
				$scope.result = [];
			}
		});
	}

	loadData();


	$scope.editMode = function(index){
		$scope.result[index].edit = true;
	}

	$scope.disclose = function(index){
		$scope.result[index].edit = false;
	}

	$scope.update = function(data){
		//console.log(data);

		var transmission = {
			'table' : 'category',
			'data' : {
				'name' : data.name
			},
			'cond' : {
				'slug' : data.slug
			}
		};

		$http({
			method : "POST",
			url    : url + "update",
			data   : transmission
		}).then(function(response){
			data.edit = false;
		});
	}

}]);